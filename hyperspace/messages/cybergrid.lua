local ComponentMessage = require('lib.encompass').ComponentMessage
local CybergridComponent = require('hyperspace.components.draw.cybergrid')

return ComponentMessage.define('CybergridMessage', {
    component = CybergridComponent
})