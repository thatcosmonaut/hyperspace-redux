local StateMessage = require('lib.encompass').StateMessage

return StateMessage.define(
    'TimeDilationStateMessage',
    {
        factor = 'number'
    }
)