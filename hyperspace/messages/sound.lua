local ComponentMessage = require('lib.encompass').ComponentMessage
local SoundComponent = require('hyperspace.components.sound')

return ComponentMessage.define('SoundMessage', {
    component = SoundComponent
})