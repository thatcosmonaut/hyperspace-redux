local ComponentMessage = require('lib.encompass').ComponentMessage
local TimeDilationComponent = require('hyperspace.components.time_dilation')

return ComponentMessage.define('TimeDilationMessage',{
    component = TimeDilationComponent
})