local ComponentMessage = require('lib.encompass').ComponentMessage
local PhysicsWorldComponent = require('hyperspace.components.physics_world')

return ComponentMessage.define('PhysicsWorldMessage', {
    component = PhysicsWorldComponent
})