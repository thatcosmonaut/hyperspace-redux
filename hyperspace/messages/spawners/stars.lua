local SpawnMessage = require('lib.encompass').SpawnMessage

return SpawnMessage.define('StarsSpawnComponent', {
    count = 'number'
})