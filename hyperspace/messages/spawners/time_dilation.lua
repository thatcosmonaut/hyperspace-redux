local SpawnMessage = require('lib.encompass').SpawnMessage

return SpawnMessage.define('TimeDilationSpawnerComponent', {
    ease_in_time = 'number',  -- time to scale into dilation
    time = 'number',          -- time to spend at full dilation
    ease_out_time = 'number', -- time to scale out of dilation,
    factor = 'number'
})