local SpawnMessage = require('lib.encompass').SpawnMessage

return SpawnMessage.define('GameOverSpawnMessage', {
    time = 'number'
})