local SpawnMessage = require('lib.encompass').SpawnMessage

return SpawnMessage.define('SoundSpawnComponent', {
    source = 'userdata'
})