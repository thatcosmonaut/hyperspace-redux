local SpawnMessage = require('lib.encompass').SpawnMessage
local Message = require('lib.encompass').Message

return SpawnMessage.define('CreateMessageTimerSpawnMessage', {
    time = 'number',
    message_to_create = Message,
    message_args = 'table'
})