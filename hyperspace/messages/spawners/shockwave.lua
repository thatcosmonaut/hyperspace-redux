local SpawnMessage = require('lib.encompass').SpawnMessage

return SpawnMessage.define('ShockwaveSpawnComponent', {
    x = 'number',
    y = 'number',
    size = 'number'
})
