local SpawnMessage = require('lib.encompass').SpawnMessage

return SpawnMessage.define('MusicSpawnMessage', {
    file = 'string',
    sections = 'table',
    bpm = 'number'
})