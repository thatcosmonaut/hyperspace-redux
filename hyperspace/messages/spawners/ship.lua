local SpawnMessage = require('lib.encompass').SpawnMessage

return SpawnMessage.define('ShipSpawnerComponent', {
    x_position = 'number',
    y_position = 'number',
    x_velocity = 'number',
    y_velocity = 'number'
})