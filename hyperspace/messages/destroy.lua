local EntityMessage = require('lib.encompass').EntityMessage

return EntityMessage.define('DestroyMessage')