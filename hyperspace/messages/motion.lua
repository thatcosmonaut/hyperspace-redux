local ComponentMessage = require('lib.encompass').ComponentMessage
local TransformComponent = require('hyperspace.components.transform')

return ComponentMessage.define(
    'MotionMessage',
    {
        component = TransformComponent,
        x_velocity = 'number',
        y_velocity = 'number',
        angular_velocity = 'number',
        instant_linear = 'boolean',
        instant_angular = 'boolean'
    }
)