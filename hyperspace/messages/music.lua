local ComponentMessage = require('lib.encompass').ComponentMessage
local MusicComponent = require('hyperspace.components.music')

return ComponentMessage.define('MusicMessage', {
    component = MusicComponent
})