local EntityMessage = require('lib.encompass').EntityMessage
local Component = require('lib.encompass').Component

return EntityMessage.define('AddComponentMessage',
    {
        component_to_add = Component,
        component_args = 'table'
    }
)