local ComponentMessage = require('lib.encompass').ComponentMessage
local Component = require('lib.encompass').Component

return ComponentMessage.define('DeactivateComponentMessage', {
    component = Component
})