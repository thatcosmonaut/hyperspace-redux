local ComponentMessage = require('lib.encompass').ComponentMessage
local ShockwaveComponent = require('hyperspace.components.draw.effects.shockwave')

return ComponentMessage.define('ShockwaveMessage',{
    component = ShockwaveComponent
})