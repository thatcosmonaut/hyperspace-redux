local ComponentMessage = require('lib.encompass').ComponentMessage
local SimpleBloomComponent = require('hyperspace.components.draw.effects.simple_bloom')

return ComponentMessage.define('SimpleBloomMessage', {
    component = SimpleBloomComponent,
    power = 'number'
})