local EntityMessage = require('lib.encompass').EntityMessage
local Component = require('lib.encompass').Component

return EntityMessage.define('RemoveComponentMessage', {
    component_to_remove = Component
})