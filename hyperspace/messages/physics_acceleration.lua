local ComponentMessage = require('lib.encompass').ComponentMessage
local PhysicsComponent = require('hyperspace.components.physics')

return ComponentMessage.define(
    'PhysicsAccelerationMessage',
    {
        component = PhysicsComponent,
        x_thrust = 'number',
        y_thrust = 'number',
        torque = 'number',
        instant_thrust = 'boolean',
        instant_torque = 'boolean'
    }
)