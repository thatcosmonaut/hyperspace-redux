local SideEffectMessage = require('lib.encompass').SideEffectMessage

return SideEffectMessage.define(
    'ParticleSystemMessage',
    {
        particle_system = 'userdata'
    }
)