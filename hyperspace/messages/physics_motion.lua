local ComponentMessage = require('lib.encompass').ComponentMessage
local PhysicsComponent = require('hyperspace.components.physics')

return ComponentMessage.define(
    'PhysicsMotionMessage',
    {
        component = PhysicsComponent,
        x_velocity = 'number',
        y_velocity = 'number',
        angular_velocity = 'number',
        instant_linear = 'boolean',
        instant_angular = 'boolean'
    }
)