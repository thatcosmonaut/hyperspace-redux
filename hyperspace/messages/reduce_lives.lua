local ComponentMessage = require('lib.encompass').ComponentMessage
local LivesComponent = require('hyperspace.components.draw.lives')

return ComponentMessage.define('LivesMessage', { component = LivesComponent })