local ComponentMessage = require('lib.encompass').ComponentMessage
local VelocityComponent = require('hyperspace.components.velocity')

return ComponentMessage.define('AccelerationMessage', {
    component = VelocityComponent,
    x_thrust = 'number',
    y_thrust = 'number',
    torque = 'number',
    instant_thrust = 'boolean',
    instant_torque = 'boolean'
})