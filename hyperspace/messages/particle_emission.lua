local SideEffectMessage = require('lib.encompass').SideEffectMessage

return SideEffectMessage.define(
    'ParticleEmissionMessage',
    {
        particle_system = 'userdata',
        x = 'number',
        y = 'number',
        amount = 'number'
    }
)