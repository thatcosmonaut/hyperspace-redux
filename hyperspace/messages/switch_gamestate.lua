local SideEffectMessage = require('lib.encompass').SideEffectMessage

return SideEffectMessage.define('SwitchGamestateMessage', {
    gamestate = 'table'
})