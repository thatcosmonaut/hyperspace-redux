local Hyperspace = {}

local MainMenu = require('hyperspace.gamestates.main_menu')
local Waves = require('hyperspace.gamestates.waves')

local current_gamestate

local marked_gamestate_switch = false
local next_gamestate

function Hyperspace.switch_gamestate(gamestate)
    marked_gamestate_switch = true
    next_gamestate = gamestate
end

function Hyperspace.__switch_gamestate(gamestate)
    if current_gamestate ~= nil then
        current_gamestate.exit()
    end

    gamestate.enter()
    current_gamestate = gamestate
end

function Hyperspace.load()
    MainMenu.initialize(Hyperspace)
    Waves.initialize(Hyperspace)
    Hyperspace.switch_gamestate(MainMenu)
end

function Hyperspace.update(dt)
    if marked_gamestate_switch then
        Hyperspace.__switch_gamestate(next_gamestate)
        marked_gamestate_switch = false
    end
    current_gamestate.update(dt)
end

function Hyperspace.draw()
    current_gamestate.draw()

    love.graphics.setBlendMode('alpha')
    love.graphics.print("FPS: " .. tostring(love.timer.getFPS()), 10, 10)
end

Hyperspace.MainMenu = MainMenu
Hyperspace.Waves = Waves

return Hyperspace