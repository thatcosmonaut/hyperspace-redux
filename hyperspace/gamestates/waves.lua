local encompass = require('lib.encompass')

local DetecterInitializer = require('hyperspace.initializers.game.detecter')
local SpawnerInitializer = require('hyperspace.initializers.game.spawner')
local ModifierInitializer = require('hyperspace.initializers.game.modifier')
local RendererInitializer = require('hyperspace.initializers.game.renderer')
local SideEffecterInitializer = require('hyperspace.initializers.game.side_effecter')

local PhysicsComponent = require('hyperspace.components.physics_world')

local ParticleSystemHelper = require('hyperspace.helpers.particle_system')
local ParticleSystemDrawComponent = require('hyperspace.components.draw.particle_system')

local CybergridComponent = require('hyperspace.components.draw.cybergrid')
local GlowDrawComponent = require('hyperspace.components.draw.effects.glow')
local ScanlinesDrawComponent = require('hyperspace.components.draw.effects.scanlines')

local AsteroidSpawnMessage = require('hyperspace.messages.spawners.asteroid')
local MusicSpawnMessage = require('hyperspace.messages.spawners.music')
local ShipSpawnMessage = require('hyperspace.messages.spawners.ship')
local StarsSpawnMessage = require('hyperspace.messages.spawners.stars')

local Waves = {}

local Hyperspace

local world
local world_canvas
local physics_world

local explode_particle_system

function Waves.initialize(hyperspace)
    Hyperspace = hyperspace

    world_canvas = love.graphics.newCanvas(
        love.graphics.getWidth(),
        love.graphics.getHeight(),
        {
            msaa = 2
        }
    )
    world = encompass.World:new()

    physics_world = love.physics.newWorld()

    explode_particle_system = ParticleSystemHelper.create_particle_system(
        require('hyperspace.prefabs.visuals.particle_systems.asteroid_explode')
    )

    local processor_args = {
        physics_world = physics_world,
        explode_particle_system = explode_particle_system,
        Hyperspace = Hyperspace,
        MainMenu = Hyperspace.MainMenu
    }

    DetecterInitializer.initialize(world)
    SpawnerInitializer.initialize(world, processor_args)
    ModifierInitializer.initialize(world, processor_args)
    RendererInitializer.initialize(world)
    SideEffecterInitializer.initialize(world, processor_args)
end

function Waves.enter()
    local particle_system_entity = world:create_entity()
    particle_system_entity:add_component(ParticleSystemDrawComponent,
        'layer',           -1,
        'particle_system', explode_particle_system
    )

    local post_processing_entity = world:create_entity()
    post_processing_entity:add_component(ScanlinesDrawComponent,
        'layer',     10000,
        'width',     1,
        'phase',     0,
        'thickness', 1,
        'opacity',   0.25,
        'color',     {0, 0, 0}
    )

    post_processing_entity:add_component(GlowDrawComponent,
        'layer',     9998,
        'strength',  5,
        'min_luma',  0.5
    )

    world:create_message(StarsSpawnMessage, 'count', 40)

    local cybergrid_entity = world:create_entity()
    cybergrid_entity:add_component(
        CybergridComponent,
        unpack(require('hyperspace.prefabs.visuals.cybergrid'))
    )

    world:create_message(
        MusicSpawnMessage,
        unpack(require('hyperspace.prefabs.music.give_you_love'))
    )

    local physics_world_entity = world:create_entity()
    physics_world_entity:add_component(PhysicsComponent,
        'physics_world', physics_world
    )

    world:create_message(ShipSpawnMessage,
        'x_position', 640,
        'y_position', 360,
        'x_velocity', 0,
        'y_velocity', 0
    )

    for _ = 1, 5 do
        world:create_message(AsteroidSpawnMessage,
            'x_position', love.math.random(1280),
            'y_position', love.math.random(720),
            'x_velocity', love.math.random(-50, 50),
            'y_velocity', love.math.random(-50, 50),
            'size',       128,
            'color',      { r = 1, g = 1, b = 1, a = 1 }
        )
    end
end

function Waves.exit()
    world:destroy_all_entities()
end

function Waves.update(dt)
    world:update(dt)
end

function Waves.draw()
    love.graphics.setCanvas(world_canvas)
    love.graphics.clear()
    love.graphics.setCanvas()
    love.graphics.clear()

    world:draw(world_canvas)

    love.graphics.setCanvas()
    love.graphics.setBlendMode('alpha', 'premultiplied')
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(world_canvas)
end

return Waves