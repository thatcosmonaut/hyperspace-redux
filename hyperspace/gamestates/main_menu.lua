local Waves = require('hyperspace.gamestates.waves')

local MainMenu = {}

local GlowDrawComponent = require('hyperspace.components.draw.effects.glow')
local ScanlinesDrawComponent = require('hyperspace.components.draw.effects.scanlines')

local CybergridComponent = require('hyperspace.components.draw.cybergrid')
local CybergridDetecter = require('hyperspace.engines.detecters.cybergrid')
local CybergridModifier = require('hyperspace.engines.modifiers.component.cybergrid')

local RendererInitializer = require('hyperspace.initializers.game.renderer')

local Hyperspace

local world
local world_canvas

local title_text
local press_any_key_text

local music

function MainMenu.initialize(hyperspace)
    Hyperspace = hyperspace

    world_canvas = love.graphics.newCanvas()
    world = require('lib.encompass').World:new()

    local title_font = love.graphics.newFont('hyperspace/assets/fonts/minimal.otf', 120)
    title_text = love.graphics.newText(title_font, 'HYPERSPACE')

    local press_any_key_font = love.graphics.newFont('hyperspace/assets/fonts/minimal.otf', 30)
    press_any_key_text = love.graphics.newText(press_any_key_font, 'Press any key to begin')

    music = love.audio.newSource('hyperspace/assets/music/hyperspace.mp3', 'stream')

    world:add_detector(CybergridDetecter)
    world:add_modifier(CybergridModifier)

    RendererInitializer.initialize(world)

    local post_processing_entity = world:create_entity()
    post_processing_entity:add_component(ScanlinesDrawComponent,
        'layer',     10000,
        'width',     1,
        'phase',     0,
        'thickness', 1,
        'opacity',   0.25,
        'color',     {0, 0, 0}
    )

    post_processing_entity:add_component(GlowDrawComponent,
        'layer',    9998,
        'strength', 5,
        'min_luma', 0.5
    )

    local cybergrid_entity = world:create_entity()
    cybergrid_entity:add_component(
        CybergridComponent,
        unpack(require('hyperspace.prefabs.visuals.cybergrid'))
    )
end

function MainMenu.enter()
    music:play()
end

function MainMenu.exit()
    music:stop()
end

function MainMenu.update(dt)
    if love.keyboard.isDown('z', 'x', 'v', 'c', 'space', 'return') then
        Hyperspace.switch_gamestate(Waves)
    end

    world:update(dt)
end

function MainMenu.draw()
    love.graphics.setCanvas(world_canvas)
    love.graphics.clear()
    love.graphics.setCanvas()
    love.graphics.clear()

    world:draw(world_canvas)

    love.graphics.setCanvas()
    love.graphics.setBlendMode('alpha', 'premultiplied')
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(world_canvas)

    love.graphics.setBlendMode('alpha')
    local text_width, text_height = title_text:getDimensions()
    love.graphics.draw(title_text, 640, 180, 0, 1, 1, text_width * 0.5, text_height * 0.5)

    text_width, text_height = press_any_key_text:getDimensions()
    love.graphics.draw(press_any_key_text, 640, 420, 0, 1, 1, text_width * 0.5, text_height * 0.5)
end

return MainMenu