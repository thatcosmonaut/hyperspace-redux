local Component = require('lib.encompass').Component

return Component.define(
    'TimeDilationCollisionResponse',
    {
        collision_types = 'table',
        ease_in_time = 'number',
        time = 'number',
        ease_out_time = 'number',
        factor = 'number'
    }
)