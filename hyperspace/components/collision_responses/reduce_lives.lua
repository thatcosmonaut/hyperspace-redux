local Component = require('lib.encompass').Component

return Component.define('ReduceLivesCollisionResponseComponent', {
    collision_types = 'table'
})