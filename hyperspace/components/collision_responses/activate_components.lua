local Component = require('lib.encompass').Component

return Component.define('ActivateComponentsCollisionResponseComponent', {
    collision_types = 'table',
    components_to_activate = 'table'
})