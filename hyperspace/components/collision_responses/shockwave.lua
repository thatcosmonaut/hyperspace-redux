local Component = require('lib.encompass').Component

return Component.define(
    'ShockwaveCollisionResponseComponent',
    {
        collision_types = 'table',
        size = 'number'
    }
)