local Component = require('lib.encompass').Component

return Component.define('PhysicsParticleCollisionResponseComponent', {
    collision_types = 'table',
    count = 'number'
})