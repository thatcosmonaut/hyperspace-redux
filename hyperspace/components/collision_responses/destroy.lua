local Component = require('lib.encompass').Component

return Component.define(
    'DestroyCollisionResponseComponent',
    {
        collision_types = 'table'
    }
)