local Component = require('lib.encompass').Component

return Component.define(
    'ParticleCollisionResponseComponent',
    {
        collision_types = 'table',
        particle_system = 'userdata',
        amount = 'number'
    }
)