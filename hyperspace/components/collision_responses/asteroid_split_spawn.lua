local Component = require('lib.encompass').Component

return Component.define(
    'AsteroidSplitSpawnCollisionResponse',
    {
        collision_types = 'table'
    }
)