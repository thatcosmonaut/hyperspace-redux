local Component = require('lib.encompass').Component

return Component.define('TemporarilyDeactivateComponentsCollisionResponseComponent', {
    collision_types = 'table',
    time = 'number',
    components_to_deactivate = 'table'
})