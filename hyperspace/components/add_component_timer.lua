local Component = require('lib.encompass').Component

return Component.define(
    'AddComponentTimerComponent',
    {
        time = 'number',
        component_to_add = Component,
        component_args = 'table'
    }
)