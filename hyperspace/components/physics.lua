local Component = require("lib.encompass").Component

local PhysicsComponent = Component.define(
    'PhysicsComponent',
    {
        body = 'userdata',
        x_offset = 'number',
        y_offset = 'number',
        x_position = 'number',
        y_position = 'number',
        rotation = 'number',
        screen_wrap = 'boolean',
        linear_velocity = 'table',
        angular_velocity = 'number',
        max_linear_speed = 'number',
        max_angular_velocity = 'number'
    }
)

function PhysicsComponent:initialize()
    self.body:setX(self.x_position)
    self.body:setY(self.y_position)
    self.body:setAngle(self.rotation)
    self.body:setLinearVelocity(self.linear_velocity.x, self.linear_velocity.y)
    self.body:setAngularVelocity(self.angular_velocity)
end

function PhysicsComponent:on_destroy()
    if not self.body:isDestroyed() then
        self.body:destroy()
    end
end

function PhysicsComponent:on_activate()
    self.body:setActive(true)
end

function PhysicsComponent:on_deactivate()
    self.body:setActive(false)
end

return PhysicsComponent