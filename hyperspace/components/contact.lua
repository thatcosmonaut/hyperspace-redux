-- this component is added to entities when
--their physics component collides with another

local Component = require('lib.encompass').Component
local Entity = require('lib.encompass').Entity

return Component.define(
    'ContactComponent',
    {
        other = Entity,
        x1 = 'number',
        y1 = 'number',
        friction = 'number',
        x_normal = 'number',
        y_normal = 'number',
        restitution = 'number'
    },
    {
        x2 = 'number',
        y2 = 'number'
    }
)