local Component = require('lib.encompass').Component

return Component.define(
    'PhysicsComponent',
    {
        physics_world = 'userdata'
    }
)