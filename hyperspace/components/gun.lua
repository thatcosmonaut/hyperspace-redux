local Component = require('lib.encompass').Component

return Component.define(
    'GunComponent',
    {
        interval = 'number'
    }
)