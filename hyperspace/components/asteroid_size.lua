local Component = require('lib.encompass').Component

return Component.define(
    'AsteroidSizeComponent',
    {
        size = 'number'
    }
)