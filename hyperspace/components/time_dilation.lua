local Component = require('lib.encompass').Component

return Component.define(
    'TimeDilationComponent',
    {
        ease_in_time = 'number',  -- time to scale into dilation
        time = 'number',            -- time to spend at full dilation
        ease_out_time = 'number', -- time to scale out of dilation,
        time_elapsed = 'number',
        factor = 'number'
    }
)