local Component = require("lib.encompass").Component

-- in LOVE, rotations are given in radians
-- the 0 rotation points along the X axis to the right
return Component.define(
    "TransformComponent",
    {
        position = 'table',
        rotation = 'number',
        forward = 'table',
        screen_wrap = 'boolean'
    }
)

