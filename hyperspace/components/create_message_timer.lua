local Component = require('lib.encompass').Component
local Message = require('lib.encompass').Message

return Component.define(
    'CreateMessageTimer',
    {
        time = 'number',
        message_to_create = Message,
        message_args = 'table'
    }
)