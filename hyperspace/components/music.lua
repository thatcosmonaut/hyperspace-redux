local Component = require('lib.encompass').Component

local MusicComponent = Component.define('MusicComponent', {
    source = 'userdata',
    time = 'number',
    bpm = 'number',
    sections = 'table'
})

function MusicComponent:on_destroy()
    self.source:stop()
end

return MusicComponent