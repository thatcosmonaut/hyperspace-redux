local Component = require("lib.encompass").Component

return Component.define(
    'VelocityComponent',
    {
        linear = 'table',
        angular = 'number',
        max_linear = 'number',
        max_angular = 'number'
    }
)