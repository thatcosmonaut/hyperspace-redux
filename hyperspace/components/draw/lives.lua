local DrawComponent = require('lib.encompass').DrawComponent

return DrawComponent.define('LivesComponent', {
    count = 'number'
})