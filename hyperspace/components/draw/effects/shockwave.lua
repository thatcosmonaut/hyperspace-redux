local DrawComponent = require('lib.encompass').DrawComponent

return DrawComponent.define('ShockwaveDrawComponent',
    {
        time_elapsed = 'number',
        shader = 'userdata'
    }
)