local DrawComponent = require('lib.encompass').DrawComponent

local SimpleBloomComponent = DrawComponent.define('SimpleBloomComponent', {
    power = 'number'
})

function SimpleBloomComponent:initialize()
    self.shader = love.graphics.newShader('hyperspace/assets/shaders/simple_bloom.frag')
    self.shader:send('texture_size', { love.graphics.getWidth(), love.graphics.getHeight() })
    self.shader:send('power', self.power)
end

return SimpleBloomComponent