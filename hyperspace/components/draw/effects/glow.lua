--[[
Public domain:
Copyright (C) 2017 by Matthias Richter <vrld@vrld.org>
Modified in 2018 by Evan Hemsley <evan@moonside.games>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.
THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
]]--

local DrawComponent = require('lib.encompass').DrawComponent

local GlowDrawComponent = DrawComponent.define('GlowDrawComponent', {
    strength = 'number',
    min_luma = 'number'
})

-- unroll convolution loop for gaussian blur shader
local function make_blur_shader(sigma)
    local support = math.max(1, math.floor(3*sigma + .5))
    local one_by_sigma_sq = sigma > 0 and 1 / (sigma * sigma) or 1
    local norm = 0

    local code = {[[
      extern vec2 direction;
      vec4 effect(vec4 color, Image texture, vec2 tc, vec2 _) {
        vec4 c = vec4(0.0f);
    ]]}
    local blur_line = "c += vec4(%f) * Texel(texture, tc + vec2(%f) * direction);"

    for i = -support,support do
      local coeff = math.exp(-.5 * i*i * one_by_sigma_sq)
      norm = norm + coeff
      code[#code+1] = blur_line:format(coeff, i)
    end

    code[#code+1] = ("return c * vec4(%f) * color;}"):format(1 / norm)

    return love.graphics.newShader(table.concat(code))
  end

function GlowDrawComponent:initialize()
    self.first_blur_shader = make_blur_shader(self.strength)
    self.first_blur_shader:send('direction', {1 / love.graphics.getWidth(), 0})
    self.second_blur_shader = make_blur_shader(self.strength)
    self.second_blur_shader:send('direction', {0, 1 / love.graphics.getHeight()})

    self.threshold_shader = love.graphics.newShader[[
      extern number min_luma;
      vec4 effect(vec4 color, Image texture, vec2 tc, vec2 _) {
        vec4 c = Texel(texture, tc);
        number luma = dot(vec3(0.299, 0.587, 0.114), c.rgb);
        return c * step(min_luma, luma) * color;
      }]]
    self.threshold_shader:send('min_luma', self.min_luma)
end

return GlowDrawComponent