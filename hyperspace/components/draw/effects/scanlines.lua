local DrawComponent = require('lib.encompass').DrawComponent

local ScanlinesDrawComponent = DrawComponent.define(
    'ScanlinesDrawComponent',
    {
        width = 'number',
        phase = 'number',
        thickness = 'number',
        opacity = 'number',
        color = 'table'
    }
)

function ScanlinesDrawComponent:initialize()
    self.shader = love.graphics.newShader('hyperspace/assets/shaders/scanlines.frag')
    self.shader:send('width', self.width)
    self.shader:send('phase', self.phase)
    self.shader:send('thickness', self.thickness)
    self.shader:send('opacity', self.opacity)
    self.shader:send('color', self.color)
end

return ScanlinesDrawComponent