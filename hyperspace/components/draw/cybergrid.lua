local PlayMat = require('lib.playmat')
local DrawComponent = require('lib.encompass').DrawComponent

local CybergridComponent = DrawComponent.define('CybergridComponent', {
    width = 'number',
    spacing = 'number',
    x_position = 'number',
    y_position = 'number',
    rotation = 'number',
    zoom = 'number',
    fov = 'number',
    offset = 'number',
    desired_speed = 'number',
    speed = 'number'
})

function CybergridComponent:initialize()
    self.grid_canvas = love.graphics.newCanvas(self.width, self.width)

    self.camera = PlayMat.newCamera(
        self.width,
        love.graphics.getHeight(),
        self.x_position,
        self.y_position,
        self.rotation,
        self.zoom,
        self.fov,
        self.offset
    )

    self.horizontal_lines = {}
    self.vertical_lines = {}

    for i = -self.width, self.width, self.spacing do
        table.insert(self.horizontal_lines, {
            x_start = -self.width*2,
            y_start = i,
            x_end = self.width*2,
            y_end = i
        })
    end

     -- padded self.width to enable infinite scrolling
    for i = -self.width*2, self.width*2, self.spacing do
        table.insert(self.vertical_lines, {
            x_start = i,
            y_start = 0,
            x_end = i,
            y_end = self.width
        })

    end
end

return CybergridComponent