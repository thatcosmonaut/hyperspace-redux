local DrawComponent = require('lib.encompass').DrawComponent

return DrawComponent.define('StarsComponent',
    {
        stars = 'table'
    }
)