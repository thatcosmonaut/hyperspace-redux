local DrawComponent = require('lib.encompass').DrawComponent

return DrawComponent.define(
    'DrawCircleComponent',
    {
        radius = 'number'
    }
)