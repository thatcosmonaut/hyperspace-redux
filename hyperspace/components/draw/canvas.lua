local DrawComponent = require('lib.encompass').DrawComponent

return DrawComponent.define(
    'DrawCanvasComponent',
    {
        canvas = 'userdata',
        w = 'number',
        h = 'number'
    }
)