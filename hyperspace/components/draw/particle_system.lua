local DrawComponent = require('lib.encompass').DrawComponent

return DrawComponent.define(
    'ParticleSystemDrawComponent',
    {
        particle_system = 'userdata'
    }
)