local Component = require('lib.encompass').Component

return Component.define('GamepadInput', { index = 'number' })