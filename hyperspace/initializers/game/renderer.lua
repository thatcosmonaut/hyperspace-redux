local RendererInitializer = {}

local renderers = require('hyperspace.initializers.game.tables.renderers')

function RendererInitializer.initialize(world)
    for _, renderer in ipairs(renderers) do
        world:add_renderer(renderer)
    end
end

return RendererInitializer