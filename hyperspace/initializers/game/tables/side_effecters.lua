return {
    require('hyperspace.engines.side_effecters.particle_emission'), {},
    require('hyperspace.engines.side_effecters.particle_system_update'), {},
    require('hyperspace.engines.side_effecters.switch_gamestate'), { 'Hyperspace' }
}