return {
    require('hyperspace.engines.spawners.asteroid'), { 'physics_world', 'explode_particle_system' },
    require('hyperspace.engines.spawners.bullet'), { 'physics_world' },
    require('hyperspace.engines.spawners.create_message_timer'), {},
    require('hyperspace.engines.spawners.game_over'), { 'MainMenu' },
    require('hyperspace.engines.spawners.physics_particle'), { 'physics_world' },
    require('hyperspace.engines.spawners.music'), {},
    require('hyperspace.engines.spawners.ship'), { 'physics_world' },
    require('hyperspace.engines.spawners.shockwave'), {},
    require('hyperspace.engines.spawners.sound'), {},
    require('hyperspace.engines.spawners.stars'), {},
    require('hyperspace.engines.spawners.time_dilation'), {},
}