return {
    require('hyperspace.engines.renderers.canvas'),
    require('hyperspace.engines.renderers.circle'),
    require('hyperspace.engines.renderers.cybergrid'),
    require('hyperspace.engines.renderers.debug_draw_physics'),
    require('hyperspace.engines.renderers.glow'),
    require('hyperspace.engines.renderers.lives'),
    require('hyperspace.engines.renderers.particle_system'),
    require('hyperspace.engines.renderers.scanlines'),
    require('hyperspace.engines.renderers.shockwave'),
    require('hyperspace.engines.renderers.simple_bloom'),
    require('hyperspace.engines.renderers.stars'),
}