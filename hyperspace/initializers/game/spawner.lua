local SpawnerInitializer = {}

local spawners_and_arg_names = require('hyperspace.initializers.game.tables.spawners')

function SpawnerInitializer.initialize(world, args)
    local spawners = {}
    local arg_names = {}

    for i, element in ipairs(spawners_and_arg_names) do
        if i % 2 == 1 then
            table.insert(spawners, element)
        else
            table.insert(arg_names, element)
        end
    end

    for i, spawner in ipairs(spawners) do
        local spawner_args = {}
        for _, name in ipairs(arg_names[i]) do
            table.insert(spawner_args, args[name])
        end
        world:add_spawner(spawner, spawner_args)
    end
end

return SpawnerInitializer