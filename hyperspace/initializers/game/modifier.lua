local ModifierInitializer = {}

local modifiers_and_arg_names = require('hyperspace.initializers.game.tables.modifiers')

function ModifierInitializer.initialize(world, args)
    local modifiers = {}
    local arg_names = {}

    for i, element in ipairs(modifiers_and_arg_names) do
        if i % 2 == 1 then
            table.insert(modifiers, element)
        else
            table.insert(arg_names, element)
        end
    end

    for i, modifier in ipairs(modifiers) do
        local modifier_args = {}
        for _, name in ipairs(arg_names[i]) do
            table.insert(modifier_args, args[name])
        end
        world:add_modifier(modifier, modifier_args)
    end
end

return ModifierInitializer