local SideEffecterInitializer = {}

local side_effecters_and_arg_names = require('hyperspace.initializers.game.tables.side_effecters')

function SideEffecterInitializer.initialize(world, args)
    local side_effecters = {}
    local arg_names = {}

    for i, element in ipairs(side_effecters_and_arg_names) do
        if i % 2 == 1 then
            table.insert(side_effecters, element)
        else
            table.insert(arg_names, element)
        end
    end

    for i, side_effecter in ipairs(side_effecters) do
        local side_effecter_args = {}
        for _, name in ipairs(arg_names[i]) do
            table.insert(side_effecter_args, args[name])
        end
        world:add_side_effecter(side_effecter, side_effecter_args)
    end
end

return SideEffecterInitializer