local DetecterInitializer = {}

local detectors = require('hyperspace.initializers.game.tables.detecters')

function DetecterInitializer.initialize(world)
    for _, detector in ipairs(detectors) do
        world:add_detector(detector)
    end
end

return DetecterInitializer