local GraphicsHelper = {}

function GraphicsHelper.glow_shape(points, r, g, b)
    love.graphics.setColor(r, g, b, 0.05)

    for i = 7, 2, -1 do
      if i == 2 then
        i = 1
        love.graphics.setColor(r, g, b, 1)
      end

      love.graphics.setLineWidth(i)

      love.graphics.polygon('line', unpack(points))
    end
end

return GraphicsHelper