local MathHelper = {}

-- cantor pairing maps two numbers to a unique number
-- useful for removing duplicate vectors from table
function MathHelper.cantorPairing(a, b)
  return (0.5 * (a + b) * (a + b + 1)) + b
end

function MathHelper.clamp(value, min, max)
  return math.max(min, math.min(value, max))
end

-- h from 0 to 360, s from 0 to 1, l from 0 to 1
function MathHelper.HSL(h, s, l, a)
	if s<=0 then return l,l,l,a end
  h, s, l = h/360*6, math.min(math.max(0, s), 1), math.min(math.max(0, l), 1)
	local c = (1-math.abs(2*l-1))*s
	local x = (1-math.abs(h%2-1))*c
	local m,r,g,b = (l-.5*c), 0,0,0
	if h < 1     then r,g,b = c,x,0
	elseif h < 2 then r,g,b = x,c,0
	elseif h < 3 then r,g,b = 0,c,x
	elseif h < 4 then r,g,b = 0,x,c
	elseif h < 5 then r,g,b = x,0,c
	else              r,g,b = c,0,x
	end return (r+m)*1,(g+m)*1,(b+m)*1,a
end

function MathHelper.randomFloat(low, high)
  return love.math.random() + love.math.random(low, high)
end

function MathHelper.lerp(a,b,t)
  return (1-t)*a + t*b
end

function MathHelper.pingPong(time, length)
  local l = 2 * length
  local t = time % l
  if (0 <= t) and (t <= length) then
    return t
  else
    return l - t
  end
end

function MathHelper.reflectVector(d, n) -- n must be normalized
  return d - (2 * (d * n) * n)
end

function MathHelper.screenX(world_x)
  return world_x / love.graphics.getWidth()
end

function MathHelper.screenY(world_y)
  return world_y / love.graphics.getHeight()
end

function MathHelper.stepToward(start_value, end_value, step)
  if (start_value < end_value) then
    return math.min(start_value + step, end_value)
  else
    return math.max(start_value - step, end_value)
  end
end

return MathHelper
