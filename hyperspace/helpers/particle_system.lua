local ParticleSystemHelper = {}

function ParticleSystemHelper.create_particle_system(args)
    local particle_canvas = love.graphics.newCanvas(1, 1)
    love.graphics.setCanvas(particle_canvas)
    love.graphics.setBlendMode('alpha')
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle('fill', 0, 0, 1, 1)
    love.graphics.setCanvas()

    local particle_system = love.graphics.newParticleSystem(particle_canvas, args.max_particles)

    particle_system:setColors(unpack(args.colors))

    particle_system:setSpread(args.spread)
    particle_system:setSpeed(args.speed_min, args.speed_max)
    particle_system:setSizes(unpack(args.sizes))
    particle_system:setSizeVariation(args.size_variation)
    particle_system:setParticleLifetime(args.lifetime_min, args.lifetime_max)

    return particle_system
end

return ParticleSystemHelper