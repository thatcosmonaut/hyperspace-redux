return {
    'file', 'hyperspace/assets/music/give_you_love.mp3',
    'bpm', 123,
    'sections', {
        {
            time = 15,
            intensity = 0.2
        },
        {
            time = 46,
            intensity = 0.3
        },
        {
            time = 78,
            intensity = 0.4
        },
        {
            time = 93,
            intensity = 0.2
        },
        {
            time = 101,
            intensity = 0.3
        },
        {
            time = 141,
            intensity = 0.6
        },
        {
            time = 156,
            intensity = 0.3
        },
        {
            intensity = 0.6
        }
    }
}