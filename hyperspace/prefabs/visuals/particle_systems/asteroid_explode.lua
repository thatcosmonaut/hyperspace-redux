local colors = {
    1, 1, 1, 1,
    1, 1, 1, 1,
    1, 1, 1, 1,
    1, 1, 1, 1,
    1, 1, 1, 1,
    1, 1, 1, 0
}

return {
    max_particles = 8000,
    colors = colors,
    spread = math.pi * 2,
    speed_min = 100,
    speed_max = 200,
    sizes = {2, 1, 0},
    size_variation = 1,
    lifetime_min = 3,
    lifetime_max = 5
}