return {
    'layer',         -90,
    'width',         love.graphics.getWidth(),
    'spacing',       128,
    'x_position',    0,
    'y_position',    love.graphics.getHeight(),
    'rotation',      math.pi * 0.5,
    'zoom',          love.graphics.getWidth(),
    'fov',           1,
    'offset',        0.5,
    'desired_speed', 0,
    'speed',         200
}