extern vec2 center;
extern float time;
extern vec3 shockParams;

vec4 effect(vec4 color, Image texture, vec2 texcoord, vec2 pixel_coords)
{
  float dist = distance(texcoord, center);
  vec2 uv = texcoord;

  if ( (dist <= (time + shockParams.z)) && (dist >= (time - shockParams.z)) )
  {
    float diff = (dist - time);
    float diff_dist = min(distance(dist, time + shockParams.z), distance(dist, time - shockParams.z));
    float powDiff = 1.0 - pow(abs(diff*shockParams.x),
                                shockParams.y * diff_dist);
    float diffTime = diff  * powDiff;
    vec2 diffUV = normalize(texcoord - center);
    uv = texcoord + (diffUV * diffTime);
    return texture2D(texture, uv) * 1.5; //brighten

  }

  return texture2D(texture, uv);
}
