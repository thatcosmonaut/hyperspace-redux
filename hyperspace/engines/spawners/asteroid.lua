local Vector = require('lib.hump.vector')
local GraphicsHelper = require('hyperspace.helpers.graphics')
local MathHelper = require('hyperspace.helpers.math')

local AsteroidCollisionType = require('hyperspace.components.collision_types.asteroid')
local BulletCollisionType = require('hyperspace.components.collision_types.bullet')
local ShipCollisionType = require('hyperspace.components.collision_types.ship')
local AsteroidSplitSpawnCollisionResponseComponent = require('hyperspace.components.collision_responses.asteroid_split_spawn')
local BounceCollisionResponseComponent = require('hyperspace.components.collision_responses.bounce')
local DestroyCollisionResponseComponent = require('hyperspace.components.collision_responses.destroy')
local ParticleCollisionResponseComponent = require('hyperspace.components.collision_responses.particle')
local PhysicsParticleCollisionResponseComponent = require('hyperspace.components.collision_responses.physics_particle')
local ShockwaveCollisionResponseComponent = require('hyperspace.components.collision_responses.shockwave')
local TemporarilyDeactivateComponentsCollisionResponseComponent = require('hyperspace.components.collision_responses.temporarily_deactivate_components')
local AsteroidSizeComponent = require('hyperspace.components.asteroid_size')
local PhysicsComponent = require('hyperspace.components.physics')
local DrawCanvasComponent = require('hyperspace.components.draw.canvas')
local TransformComponent = require('hyperspace.components.transform')
local UpdateFromPositionAndVelocityComponent = require('hyperspace.components.update_from_position_and_velocity')
local VelocityComponent = require('hyperspace.components.velocity')

local AsteroidSpawnMessage = require('hyperspace.messages.spawners.asteroid')

local Spawner = require('lib.encompass').Spawner
local AsteroidSpawner = Spawner.define('AsteroidSpawner', AsteroidSpawnMessage)

local function generate_points(size)
    local points = {}

    local start_point = Vector(0, 0)
    local angle = 0
    local point_count = love.math.random(5,8)

    local small = false
    for _ = 1, point_count do
      local distance
      if small or love.math.random() < 0.8 then
        distance = MathHelper.randomFloat(size * 0.25, size * 0.5)
      else
        distance = MathHelper.randomFloat(1, size * 0.25)
        small = true
      end

      local point = start_point + Vector(0,1):rotated(angle) * distance
      table.insert(points, Vector(math.floor(point.x), math.floor(point.y)))
      angle = angle + ((math.pi * 2) / point_count)
    end

    local unrolled_points = {}
    for _, vector in ipairs(points) do
        table.insert(unrolled_points, vector.x)
        table.insert(unrolled_points, vector.y)
    end

    return unrolled_points
end

function AsteroidSpawner:initialize(physics_world, explode_particle_system)
    self.physics_world = physics_world
    self.explode_particle_system = explode_particle_system
end

function AsteroidSpawner:create_physics_body(entity, x_position, y_position, triangles)
    local collision_body = love.physics.newBody(self.physics_world, x_position, y_position, 'dynamic')
    collision_body:setMass(10)
    collision_body:setLinearVelocity(0, 0)
    collision_body:setAngularVelocity(0, 0)

    for _, triangle in pairs(triangles) do
      local collision_shape = love.physics.newPolygonShape(triangle)
      local collision_fixture = love.physics.newFixture(collision_body, collision_shape)
      collision_fixture:setUserData(entity)
      collision_fixture:setCategory(2)
    end

    return collision_body
end

function AsteroidSpawner:create_asteroid(x_position, y_position, x_velocity, y_velocity, size, color)
    local angular_velocity = 1
    local max_linear = math.huge
    local max_angular = math.huge

    local asteroid = self:create_entity()

    local asteroid_points = generate_points(size)
    local polygon_triangles = love.math.triangulate(asteroid_points)

    local collision_body = self:create_physics_body(asteroid, x_position, y_position, polygon_triangles)

    local draw_points = {}
    for _, point in ipairs(asteroid_points) do
        table.insert(draw_points, point + size * 0.5)
    end

    local asteroid_canvas = love.graphics.newCanvas(size, size, { msaa = 8 })

    local r, g, b, a = love.graphics.getColor()
    love.graphics.setCanvas(asteroid_canvas)
    love.graphics.setBlendMode('alpha')
    love.graphics.setColor(0, 0, 0, 1)
    love.graphics.polygon('fill', unpack(draw_points))
    love.graphics.setColor(1, color.r, color.g, color.b)
    love.graphics.polygon('line', unpack(draw_points))
    --GraphicsHelper.glow_shape(draw_points, color.r, color.g, color.b)
    love.graphics.setColor(r, g, b, a)
    love.graphics.setCanvas()

    asteroid:add_component(TransformComponent,
        'position',    { x = x_position, y = y_position },
        'rotation',    0,
        'forward',     { x = 1, y = 0 },
        'screen_wrap', true
    )

    asteroid:add_component(VelocityComponent,
        'linear',      { x = x_velocity, y = y_velocity },
        'angular',     angular_velocity,
        'max_angular', max_angular,
        'max_linear',  max_linear
    )

    asteroid:add_component(DrawCanvasComponent,
        'layer', 0,
        'canvas', asteroid_canvas,
        'w', size,
        'h', size
    )

    asteroid:add_component(PhysicsComponent,
        'body',                 collision_body,
        'x_offset',             size * 0.5,
        'y_offset',             size * 0.5,
        'x_position',           x_position,
        'y_position',           y_position,
        'rotation',             0,
        'screen_wrap',          true,
        'linear_velocity',      { x = x_velocity, y = y_velocity },
        'angular_velocity',     angular_velocity,
        'max_linear_speed',     max_linear,
        'max_angular_velocity', max_angular
    )

    asteroid:add_component(UpdateFromPositionAndVelocityComponent)
    asteroid:add_component(AsteroidCollisionType)
    asteroid:add_component(AsteroidSizeComponent, 'size', size)

    local bounce_collision_component = asteroid:add_component(BounceCollisionResponseComponent,
        'collision_types', { AsteroidCollisionType }
    )

    asteroid:add_component(DestroyCollisionResponseComponent,
        'collision_types', { BulletCollisionType, ShipCollisionType }
    )

    asteroid:add_component(AsteroidSplitSpawnCollisionResponseComponent,
        'collision_types', { BulletCollisionType, ShipCollisionType }
    )

    asteroid:add_component(ParticleCollisionResponseComponent,
        'collision_types', { BulletCollisionType, ShipCollisionType },
        'particle_system', self.explode_particle_system,
        'amount', 400
    )

    asteroid:add_component(PhysicsParticleCollisionResponseComponent,
        'collision_types', { BulletCollisionType, ShipCollisionType },
        'count', 5
    )

    asteroid:add_component(ShockwaveCollisionResponseComponent,
        'collision_types', { AsteroidCollisionType },
        'size', 10
    )

    asteroid:add_component(ShockwaveCollisionResponseComponent,
        'collision_types', { BulletCollisionType, ShipCollisionType },
        'size', size
    )

    asteroid:add_component(TemporarilyDeactivateComponentsCollisionResponseComponent,
        'collision_types', { AsteroidCollisionType },
        'time',            1,
        'components_to_deactivate', {
            bounce_collision_component
        }
    )
end

function AsteroidSpawner:spawn(asteroid_spawn_message)
    local x_position = asteroid_spawn_message.x_position
    local y_position = asteroid_spawn_message.y_position
    local x_velocity = asteroid_spawn_message.x_velocity
    local y_velocity = asteroid_spawn_message.y_velocity
    local size = asteroid_spawn_message.size
    local color = asteroid_spawn_message.color

    self:create_asteroid(x_position, y_position, x_velocity, y_velocity, size, color)
end

return AsteroidSpawner
