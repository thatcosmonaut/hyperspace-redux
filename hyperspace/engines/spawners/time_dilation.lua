local TimeDilationComponent = require('hyperspace.components.time_dilation')
local CreateMessageTimerComponent = require('hyperspace.components.create_message_timer')
local DestroyMessage = require('hyperspace.messages.destroy')

local TimeDilationSpawnMessage = require('hyperspace.messages.spawners.time_dilation')

local Spawner = require('lib.encompass').Spawner
local TimeDilationSpawner = Spawner.define('TimeDilationSpawner', TimeDilationSpawnMessage)

function TimeDilationSpawner:spawn(time_dilation_spawn_message)
    local ease_in_time = time_dilation_spawn_message.ease_in_time
    local time = time_dilation_spawn_message.time
    local ease_out_time = time_dilation_spawn_message.ease_out_time
    local factor = time_dilation_spawn_message.factor

    local entity = self:create_entity()

    entity:add_component(TimeDilationComponent,
        'time_elapsed',  0,
        'ease_in_time',  ease_in_time,
        'time',          time,
        'ease_out_time', ease_out_time,
        'factor',        factor
    )

    entity:add_component(CreateMessageTimerComponent,
        'time',               ease_in_time + time + ease_out_time,
        'message_to_create',  DestroyMessage,
        'message_args', {
             'entity',        entity
        }
    )
end

return TimeDilationSpawner