local PARTICLE_MAX = 100

local vector = require('lib.hump.vector-light')

local TransformComponent = require('hyperspace.components.transform')
local VelocityComponent = require('hyperspace.components.velocity')
local PhysicsComponent = require('hyperspace.components.physics')
local UpdateFromPhysicsComponent = require('hyperspace.components.update_from_physics')
local DrawCircleComponent = require('hyperspace.components.draw.circle')
local CreateMessageTimerComponent = require('hyperspace.components.create_message_timer')
local DeactivateMessage = require('hyperspace.messages.deactivate')

local PhysicsParticleSpawnMessage = require('hyperspace.messages.spawners.physics_particle')

local PooledSpawner = require('lib.encompass').PooledSpawner
local PhysicsParticleSpawner = PooledSpawner.define('PhysicsParticleSpawner', PhysicsParticleSpawnMessage, PARTICLE_MAX, PooledSpawner.OverflowBehaviors.cycle)

function PhysicsParticleSpawner:initialize(physics_world)
    self.physics_world = physics_world
end

function PhysicsParticleSpawner:create_physics_body(entity, x_position, y_position, x_velocity, y_velocity)
    local collision_body = love.physics.newBody(self.physics_world, x_position, y_position, 'dynamic')
    collision_body:setMass(0.0001)
    collision_body:setLinearVelocity(x_velocity, y_velocity)
    collision_body:setAngularVelocity(0, 0)

    local collision_shape = love.physics.newRectangleShape(1, 1)
    local collision_fixture = love.physics.newFixture(collision_body, collision_shape)

    collision_fixture:setUserData(entity)
    collision_fixture:setCategory(2)
    collision_fixture:setMask(1)
    return collision_body
end

function PhysicsParticleSpawner:generate(particle_entity)
    local collision_body = self:create_physics_body(particle_entity, 0, 0, 0, 0)

    particle_entity:add_component(TransformComponent,
        'position',    { x = 0, y = 0 },
        'rotation',    0,
        'forward',     { x = 0, y = 1 },
        'screen_wrap', true
    )

    particle_entity:add_component(VelocityComponent,
        'linear',      { x = 0, y = 0 },
        'angular',     0,
        'max_angular', math.huge,
        'max_linear',  math.huge
    )

    particle_entity:add_component(PhysicsComponent,
        'body',                 collision_body,
        'x_offset',             0,
        'y_offset',             0,
        'x_position',           0,
        'y_position',           0,
        'rotation',             0,
        'screen_wrap',          true,
        'linear_velocity',      { x = 0, y = 0 },
        'angular_velocity',     0,
        'max_linear_speed',     math.huge,
        'max_angular_velocity', math.huge
    )

    particle_entity:add_component(DrawCircleComponent,
        'layer', 0,
        'radius', 1
    )

    particle_entity:add_component(UpdateFromPhysicsComponent)

    particle_entity:add_component(CreateMessageTimerComponent,
        'time',              10,
        'message_to_create', DeactivateMessage,
        'message_args', {
            'entity',        particle_entity
        }
    )
end

function PhysicsParticleSpawner:spawn(particle_entity, physics_particle_spawn_message)
    local x_position = physics_particle_spawn_message.x_position
    local y_position = physics_particle_spawn_message.y_position
    local x_velocity = physics_particle_spawn_message.x_velocity
    local y_velocity = physics_particle_spawn_message.y_velocity

    local transform_component = particle_entity:get_component(TransformComponent)
    local velocity_component = particle_entity:get_component(VelocityComponent)
    local collision_component = particle_entity:get_component(PhysicsComponent)
    local body = collision_component.body

    transform_component.position.x = x_position
    transform_component.position.y = y_position
    velocity_component.linear.x = x_velocity
    velocity_component.linear.y = y_velocity
    collision_component.x_position = x_position
    collision_component.y_position = y_position
    collision_component.linear_velocity.x = x_velocity
    collision_component.linear_velocity.y = y_velocity
    body:setX(x_position)
    body:setY(y_position)
    body:setLinearVelocity(x_velocity, y_velocity)
    if particle_entity:has_component(CreateMessageTimerComponent) then
        particle_entity:get_component(CreateMessageTimerComponent).time = 10
    else
        particle_entity:add_component(CreateMessageTimerComponent,
            'time',              10,
            'message_to_create', DeactivateMessage,
            'message_args', {
                'entity',        particle_entity
            }
        )
    end
end

return PhysicsParticleSpawner