local StarsComponent = require('hyperspace.components.draw.stars')
local StarSpawnMessage = require('hyperspace.messages.spawners.stars')

local Spawner = require('lib.encompass').Spawner
local StarSpawner = Spawner.define('StarSpawner', StarSpawnMessage)

function StarSpawner:create_stars(count)
    local stars_entity = self.world:create_entity()

    local stars = {}

    for _ = 1, count do
        local star = {
            r = love.math.random(),
            g = love.math.random(),
            b = love.math.random(),
            a = 1,
            x = love.math.random(1280),
            y = love.math.random(720)
        }
        table.insert(stars, star)
    end

    stars_entity:add_component(StarsComponent,
        'layer', -100,
        'stars', stars
    )

    return stars_entity
end

function StarSpawner:spawn(star_spawn_message)
    local count = star_spawn_message.count

    self:create_stars(count)
end

return StarSpawner