local CreateMessageTimerSpawnMessage = require('hyperspace.messages.spawners.create_message_timer')
local CreateMessageTimerComponent = require('hyperspace.components.create_message_timer')

local Spawner = require('lib.encompass').Spawner
local CreateMessageTimerSpawner = Spawner.define('CreateMessageTimerSpawner', CreateMessageTimerSpawnMessage)

function CreateMessageTimerSpawner:spawn(message)
    local entity = self:create_entity()
    entity:add_component(CreateMessageTimerComponent,
        'time', message.time,
        'message_to_create', message.message_to_create,
        'message_args', message.message_args
    )
end

return CreateMessageTimerSpawner