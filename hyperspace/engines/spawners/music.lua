local MusicComponent = require('hyperspace.components.music')
local SimpleBloomComponent = require('hyperspace.components.draw.effects.simple_bloom')
local MusicSpawnMessage = require('hyperspace.messages.spawners.music')

local Spawner = require('lib.encompass').Spawner
local MusicSpawner = Spawner.define('MusicSpawner', MusicSpawnMessage)

function MusicSpawner:spawn(message)
    local source = love.audio.newSource(message.file, 'stream')

    local entity = self:create_entity()
    entity:add_component(MusicComponent,
        'source',   source,
        'time',     0,
        'bpm',      message.bpm,
        'sections', message.sections
    )

    entity:add_component(SimpleBloomComponent,
        'layer', 9999,
        'power', 0.5
    )

    source:play()
end

return MusicSpawner