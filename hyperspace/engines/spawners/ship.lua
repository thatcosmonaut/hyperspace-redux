local vector = require('lib.hump.vector-light')

local TransformComponent = require("hyperspace.components.transform")
local VelocityComponent = require("hyperspace.components.velocity")
local GamepadInputComponent = require("hyperspace.components.gamepad_input")
local KeyboardInputComponent = require('hyperspace.components.keyboard_input')
local DrawCanvasComponent = require("hyperspace.components.draw.canvas")
local UpdateFromPositionAndVelocityComponent = require(
    'hyperspace.components.update_from_position_and_velocity'
)
local LivesComponent = require('hyperspace.components.draw.lives')
local GunComponent = require('hyperspace.components.gun')
local CanBlinkComponent = require('hyperspace.components.can_blink')
local CanShootComponent = require('hyperspace.components.can_shoot')
local PhysicsComponent = require('hyperspace.components.physics')
local TemporarilyDeactivateComponentsCollisionResponseComponent = require('hyperspace.components.collision_responses.temporarily_deactivate_components')
local ReduceLivesCollisionResponseComponent = require('hyperspace.components.collision_responses.reduce_lives')
local TimeDilationCollisionResponseComponent = require('hyperspace.components.collision_responses.time_dilation')
local ShockwaveCollisionResponseComponent = require('hyperspace.components.collision_responses.shockwave')
local AsteroidCollisionType = require('hyperspace.components.collision_types.asteroid')
local ShipCollisionType = require('hyperspace.components.collision_types.ship')
local DebugDrawPhysicsComponent = require('hyperspace.components.draw.debug_draw_physics')

local ShipSpawnMessage = require('hyperspace.messages.spawners.ship')

local Spawner = require('lib.encompass').Spawner
local ShipSpawner = Spawner.define('ShipSpawner', ShipSpawnMessage)

function ShipSpawner:initialize(physics_world)
    self.physics_world = physics_world
    self:initialize_canvas()
end

function ShipSpawner:initialize_canvas()
    self.ship_canvas = love.graphics.newCanvas(32, 32, {format = 'normal', msaa = 4})
    love.graphics.setCanvas(self.ship_canvas)
      love.graphics.clear()
      love.graphics.setLineJoin('bevel')
      love.graphics.setBlendMode('alpha')
      love.graphics.setColor(0, 0, 0, 1)
      love.graphics.polygon('fill', 16-6, 16-6, 16+0, 16+12, 16+6, 16-6, 16-6, 16-6)
      love.graphics.setColor(1, 1, 1, 1)
      love.graphics.line(16-6, 16-6, 16+0, 16+12, 16+6, 16-6, 16-6, 16-6)
    love.graphics.setCanvas()
end

function ShipSpawner:create_physics_body(entity, x_position, y_position, rotation, x_velocity, y_velocity)
    local collision_body = love.physics.newBody(self.physics_world, x_position, y_position, 'dynamic')
    collision_body:setMass(1)
    collision_body:setAngle(rotation)
    collision_body:setLinearVelocity(x_velocity, y_velocity)
    collision_body:setAngularVelocity(0)
    local collision_shape = love.physics.newPolygonShape(-6, -6, 0, 12, 6, -6)
    local collision_fixture = love.physics.newFixture(collision_body, collision_shape)
    collision_fixture:setUserData(entity)
    return collision_body
end

function ShipSpawner:create_ship(x_position, y_position, x_velocity, y_velocity)
    local rotation = math.pi

    local ship = self.world:create_entity()
    local collision_body = self:create_physics_body(ship, x_position, y_position, rotation, x_velocity, y_velocity)

    -- in the future we want to tie one input method to a ship at creation time
    --ship:add_component(GamepadInputComponent, { index = 1 })

    ship:add_component(KeyboardInputComponent)
    ship:add_component(GunComponent, 'interval', 0.5)
    ship:add_component(CanBlinkComponent)
    ship:add_component(CanShootComponent)

    local x_forward, y_forward = vector.rotate(rotation, 0, 1)

    local transform_component = ship:add_component(TransformComponent,
        'position',    { x = x_position, y = y_position },
        'rotation',    rotation,
        'forward',     { x = x_forward, y = y_forward },
        'screen_wrap', true
    )

    local velocity_component = ship:add_component(VelocityComponent,
        'linear',      { x = x_velocity, y = y_velocity },
        'angular',     0,
        'max_linear',  600,
        'max_angular', math.pi * 2
    )

    local physics_component = ship:add_component(PhysicsComponent,
        'body',                 collision_body,
        'x_offset',             0,
        'y_offset',             0,
        'x_position',           x_position,
        'y_position',           y_position,
        'rotation',             rotation,
        'screen_wrap',          true,
        'linear_velocity',      { x = x_position, y = y_position },
        'angular_velocity',     0,
        'max_linear_speed',     600,
        'max_angular_velocity', math.pi * 2
    )

    ship:add_component(LivesComponent,
        'layer', 1000,
        'count', 1
    )

    local draw_canvas_component = ship:add_component(DrawCanvasComponent,
        'layer',  0,
        'canvas', self.ship_canvas,
        'w',      32,
        'h',      32
    )

    ship:add_component(UpdateFromPositionAndVelocityComponent)
    ship:add_component(ShockwaveCollisionResponseComponent,
        'collision_types', { AsteroidCollisionType },
        'size',            128
    )

    ship:add_component(TemporarilyDeactivateComponentsCollisionResponseComponent,
        'collision_types',          { AsteroidCollisionType },
        'time',                     5,
        'components_to_deactivate', {
            transform_component,
            velocity_component,
            physics_component,
            draw_canvas_component
        }
    )

    ship:add_component(ReduceLivesCollisionResponseComponent,
        'collision_types', { AsteroidCollisionType }
    )

    ship:add_component(TimeDilationCollisionResponseComponent,
        'collision_types', { AsteroidCollisionType },
        'ease_in_time',    0.25,
        'time',            1.5,
        'ease_out_time',   1.5,
        'factor',          0.02
    )

    ship:add_component(ShipCollisionType)

    return ship
end

function ShipSpawner:spawn(ship_spawn_message)
    local x_position = ship_spawn_message.x_position
    local y_position = ship_spawn_message.y_position
    local x_velocity = ship_spawn_message.x_velocity
    local y_velocity = ship_spawn_message.y_velocity

    self:create_ship(x_position, y_position, x_velocity, y_velocity)
end

return ShipSpawner