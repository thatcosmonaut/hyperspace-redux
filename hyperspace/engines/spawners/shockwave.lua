local ShockwaveDrawComponent = require('hyperspace.components.draw.effects.shockwave')
local ShockwaveSpawnMessage = require('hyperspace.messages.spawners.shockwave')

local CreateMessageTimerComponent = require('hyperspace.components.create_message_timer')
local DestroyMessage = require('hyperspace.messages.destroy')

local Spawner = require('lib.encompass').Spawner
local ShockwaveSpawner = Spawner.define('ShockwaveSpawner', ShockwaveSpawnMessage)

function ShockwaveSpawner:create_shockwave(x, y, size)
    local shockwave = self:create_entity()

    local params
    if size >= 128 then
        params = {13.0, 0.95, 0.25}
    elseif size >= 64 then
        params = {11.0, 0.9, 0.2}
    elseif size >= 16 then
        params = {10.0, 0.8, 0.15}
    else
        params = {8.0, 0.5, 0.05}
    end

    local shader = love.graphics.newShader('hyperspace/assets/shaders/shockwave.frag')
    shader:send('center', {x / love.graphics.getWidth(), y / love.graphics.getHeight()})
    shader:send('shockParams', params)

    shockwave:add_component(ShockwaveDrawComponent,
        'layer',        100,
        'shader',       shader,
        'time_elapsed', 0
    )

    shockwave:add_component(CreateMessageTimerComponent,
        'time',              1.5,
        'message_to_create', DestroyMessage,
        'message_args', {
            'entity',        shockwave
        }
    )

    return shockwave
end

function ShockwaveSpawner:spawn(shockwave_spawn_message)
    local x = shockwave_spawn_message.x
    local y = shockwave_spawn_message.y
    local size = shockwave_spawn_message.size

    self:create_shockwave(x, y, size)
end

return ShockwaveSpawner