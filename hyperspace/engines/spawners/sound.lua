local SoundComponent = require('hyperspace.components.sound')
local SoundSpawnMessage = require('hyperspace.messages.spawners.sound')

local Spawner = require('lib.encompass').Spawner
local SoundSpawner = Spawner.define('SoundSpawner', SoundSpawnMessage)

function SoundSpawner:spawn(sound_spawn_message)
    local source = sound_spawn_message.source

    local sound_entity = self:create_entity()
    sound_entity:add_component(SoundComponent,
        'source', source
    )

    source:play()
end

return SoundSpawner