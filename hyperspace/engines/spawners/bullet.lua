local TransformComponent = require('hyperspace.components.transform')
local VelocityComponent = require('hyperspace.components.velocity')
local DrawCanvasComponent = require('hyperspace.components.draw.canvas')
local PhysicsComponent = require('hyperspace.components.physics')
local UpdateFromPositionAndVelocityComponent = require(
    'hyperspace.components.update_from_position_and_velocity'
)
local TimeDilationCollisionResponseComponent = require('hyperspace.components.collision_responses.time_dilation')
local DestroyCollisionResponseComponent = require('hyperspace.components.collision_responses.destroy')
local CreateMessageTimerComponent = require('hyperspace.components.create_message_timer')
local DestroyMessage = require('hyperspace.messages.destroy')
local BulletCollisionType = require('hyperspace.components.collision_types.bullet')
local AsteroidCollisionType = require('hyperspace.components.collision_types.asteroid')

local BulletSpawnMessage = require('hyperspace.messages.spawners.bullet')

local Spawner = require('lib.encompass').Spawner
local BulletSpawner = Spawner.define('BulletSpawner', BulletSpawnMessage)

function BulletSpawner:initialize(physics_world)
    self.physics_world = physics_world
    self:initialize_canvas()
end

function BulletSpawner:initialize_canvas()
    self.canvas = love.graphics.newCanvas(4, 4)
    love.graphics.setCanvas(self.canvas)
    love.graphics.setBlendMode('alpha')
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle('fill', 0, 0, 4, 4)
    love.graphics.setCanvas()
end

function BulletSpawner:create_physics_body(entity, x_position, y_position, x_velocity, y_velocity)
    local collision_body = love.physics.newBody(self.physics_world, x_position, y_position, 'dynamic')
    collision_body:setMass(1)
    collision_body:setLinearVelocity(x_velocity, y_velocity)
    collision_body:setAngularVelocity(0)
    collision_body:setBullet(true)

    local collision_shape = love.physics.newRectangleShape(4, 4)
    local collision_fixture = love.physics.newFixture(collision_body, collision_shape)
    collision_fixture:setUserData(entity)
    collision_fixture:setCategory(1)
    collision_fixture:setMask(1)
    return collision_body
end

function BulletSpawner:create_bullet(x_position, y_position, x_velocity, y_velocity)
    local bullet = self.world:create_entity()

    local collision_body = self:create_physics_body(bullet, x_position, y_position, x_velocity, y_velocity)

    bullet:add_component(TransformComponent,
        'position',    { x = x_position, y = y_position },
        'rotation',    0,
        'forward',     { x = 1, y = 0 },
        'screen_wrap', true
    )

    bullet:add_component(VelocityComponent,
        'linear',      { x = x_velocity, y = y_velocity },
        'angular',     0,
        'max_angular', math.huge,
        'max_linear',  math.huge
    )

    bullet:add_component(PhysicsComponent,
        'body',                 collision_body,
        'x_offset',             2,
        'y_offset',             2,
        'x_position',           x_position,
        'y_position',           y_position,
        'rotation',             0,
        'screen_wrap',          true,
        'linear_velocity',      { x = x_velocity, y = y_velocity },
        'angular_velocity',     0,
        'max_linear_speed',     math.huge,
        'max_angular_velocity', math.huge
    )

    bullet:add_component(DrawCanvasComponent,
        'layer', 0,
        'canvas', self.canvas,
        'w', 4,
        'h', 4
    )

    bullet:add_component(UpdateFromPositionAndVelocityComponent)
    bullet:add_component(BulletCollisionType)
    bullet:add_component(DestroyCollisionResponseComponent,
        'collision_types', { AsteroidCollisionType }
    )

    bullet:add_component(CreateMessageTimerComponent,
        'time',              1,
        'message_to_create', DestroyMessage,
        'message_args', {
            'entity',        bullet
        }
    )
end

function BulletSpawner:spawn(bullet_spawn_message)
    local x_position = bullet_spawn_message.x_position
    local y_position = bullet_spawn_message.y_position
    local x_velocity = bullet_spawn_message.x_velocity
    local y_velocity = bullet_spawn_message.y_velocity

    self:create_bullet(x_position, y_position, x_velocity, y_velocity)
end

return BulletSpawner