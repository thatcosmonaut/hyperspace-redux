local GameOverSpawnMessage = require('hyperspace.messages.spawners.game_over')
local SwitchGamestateMessage = require('hyperspace.messages.switch_gamestate')

local CreateMessageTimerComponent = require('hyperspace.components.create_message_timer')

local Spawner = require('lib.encompass').Spawner
local GameOverSpawner = Spawner.define('GameOverSpawner', GameOverSpawnMessage)

function GameOverSpawner:initialize(MainMenu)
    self.MainMenu = MainMenu
end

function GameOverSpawner:spawn(message)
    local game_over_entity = self:create_entity()
    game_over_entity:add_component(CreateMessageTimerComponent,
        'time',              message.time,
        'message_to_create', SwitchGamestateMessage,
        'message_args', {
            'gamestate',     self.MainMenu
        }
    )
end

return GameOverSpawner