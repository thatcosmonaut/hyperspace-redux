local MusicMessage = require('hyperspace.messages.music')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local ComponentModifier = require('lib.encompass').ComponentModifier
local MusicModifier = ComponentModifier.define(
    'MusicModifier',
    MusicMessage,
    { TimeDilationStateMessage }
)

function MusicModifier:modify(music_component, frozen_fields, message, dt)
    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)

    local factor = 1
    if time_dilation_state_message ~= nil then
        factor = time_dilation_state_message.factor
    end

    dt = dt * factor

    local source = frozen_fields.source
    source:setPitch(factor)
    music_component.time = frozen_fields.time + dt
end

return MusicModifier