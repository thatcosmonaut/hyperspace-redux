local ReduceLivesMessage = require('hyperspace.messages.reduce_lives')

local ComponentModifier = require('lib.encompass').ComponentModifier
local ReduceLivesModifier = ComponentModifier.define(
    'ReduceLivesModifier',
    ReduceLivesMessage
)

function ReduceLivesModifier:modify(lives_component, frozen_fields, message, dt)
    lives_component.count = frozen_fields.count - 1
end

return ReduceLivesModifier