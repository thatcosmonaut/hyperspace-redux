local vector = require('lib.hump.vector-light')
local MathHelper = require('hyperspace.helpers.math')

local AccelerationMessage = require('hyperspace.messages.acceleration')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local MultiMessageComponentModifier = require('lib.encompass').MultiMessageComponentModifier
local AccelerationModifier = MultiMessageComponentModifier.define(
    'AccelerationModifier',
    AccelerationMessage,
    { TimeDilationStateMessage }
)

function AccelerationModifier:modify(velocity_component, frozen_fields, messages, dt)
    -- maybe consider not grouping things in tables... the reference is dangerous
    local new_x_linear, new_y_linear = frozen_fields.linear.x, frozen_fields.linear.y
    local new_angular = frozen_fields.angular
    local max_linear = frozen_fields.max_linear
    local max_angular = frozen_fields.max_angular

    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)
    dt = dt * (time_dilation_state_message ~= nil and time_dilation_state_message.factor or 1)

    for _, message in pairs(messages) do
        local instant_thrust_or_dt = message.instant_thrust and 1 or dt
        local instant_torque_or_dt = message.instant_torque and 1 or dt
        local x_thrust_adjusted, y_thrust_adjusted = vector.mul(instant_thrust_or_dt, message.x_thrust, message.y_thrust)
        new_x_linear, new_y_linear = vector.add(new_x_linear, new_y_linear, x_thrust_adjusted, y_thrust_adjusted)
        new_angular = new_angular + message.torque * instant_torque_or_dt
    end

    velocity_component.linear.x, velocity_component.linear.y = vector.trim(max_linear, new_x_linear, new_y_linear)
    velocity_component.angular = MathHelper.clamp(new_angular, -max_angular, max_angular)
end

return AccelerationModifier