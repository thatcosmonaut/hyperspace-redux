local ShockwaveMessage = require('hyperspace.messages.shockwave')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local ComponentModifier = require('lib.encompass').ComponentModifier
local ShockwaveModifier = ComponentModifier.define(
    'ShockwaveModifier',
    ShockwaveMessage,
    { TimeDilationStateMessage }
)

function ShockwaveModifier:modify(shockwave_component, frozen_fields, message, dt)
    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)
    dt = dt * (time_dilation_state_message ~= nil and time_dilation_state_message.factor or 1)

    local new_time_elapsed = frozen_fields.time_elapsed + dt
    shockwave_component.time_elapsed = new_time_elapsed
    shockwave_component.shader:send('time', new_time_elapsed)
end

return ShockwaveModifier