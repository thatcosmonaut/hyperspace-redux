local SimpleBloomMessage = require('hyperspace.messages.simple_bloom')

local ComponentModifier = require('lib.encompass').ComponentModifier
local SimpleBloomModifier = ComponentModifier.define(
    'SimpleBloomModifier',
    SimpleBloomMessage
)

function SimpleBloomModifier:modify(bloom_component, frozen_fields, message, dt)
    bloom_component.power = message.power
    bloom_component.shader:send('power', message.power)
end

return SimpleBloomModifier