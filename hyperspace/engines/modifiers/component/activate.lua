local ActivateComponentMessage = require('hyperspace.messages.activate_component')

local ComponentModifier = require('lib.encompass').ComponentModifier
local ActivateComponentModifier = ComponentModifier.define(
    'ActivateComponentModifier',
    ActivateComponentMessage
)

function ActivateComponentModifier:modify(component, frozen_fields, message, dt)
    component:activate()
end

return ActivateComponentModifier