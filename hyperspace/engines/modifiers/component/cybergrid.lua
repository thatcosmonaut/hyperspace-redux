local CybergridMessage = require('hyperspace.messages.cybergrid')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local ComponentModifier = require('lib.encompass').ComponentModifier
local CybergridModifier = ComponentModifier.define(
    'CybergridModifier',
    CybergridMessage,
    { TimeDilationStateMessage }
)

function CybergridModifier:modify(cybergrid_component, frozen_fields, cybergrid_message, dt)
    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)
    dt = dt * (time_dilation_state_message ~= nil and time_dilation_state_message.factor or 1)

    local x = frozen_fields.x_position
    local speed = frozen_fields.speed * dt
    local loop_width = frozen_fields.width * 0.5

    local new_x = (x - speed) % loop_width
    cybergrid_component.x_position = new_x
    cybergrid_component.camera.x = new_x
end

return CybergridModifier