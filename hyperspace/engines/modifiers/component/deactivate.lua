local DeactivateComponentMessage = require('hyperspace.messages.deactivate_component')

local ComponentModifier = require('lib.encompass').ComponentModifier
local DeactivateComponentModifier = ComponentModifier.define(
    'DeactivateComponentModifier',
    DeactivateComponentMessage
)

function DeactivateComponentModifier:modify(component, frozen_fields, message, dt)
    component:deactivate()
end

return DeactivateComponentModifier