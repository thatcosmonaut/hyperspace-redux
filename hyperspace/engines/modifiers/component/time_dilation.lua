local TimeDilationMessage = require('hyperspace.messages.time_dilation')

local ComponentModifier = require('lib.encompass').ComponentModifier
local TimeDilationModifier = ComponentModifier.define('TimeDilationModifier', TimeDilationMessage)

function TimeDilationModifier:modify(time_dilation_component, frozen_fields, message, dt)
    time_dilation_component.time_elapsed = frozen_fields.time_elapsed + dt
end

return TimeDilationModifier