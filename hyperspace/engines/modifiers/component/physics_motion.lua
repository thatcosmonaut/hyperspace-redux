local PhysicsMotionMessage = require('hyperspace.messages.physics_motion')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local MultiMessageComponentModifier = require('lib.encompass').MultiMessageComponentModifier
local PhysicsMotionModifier = MultiMessageComponentModifier.define(
    'PhysicsMotionModifier',
    PhysicsMotionMessage,
    { TimeDilationStateMessage }
)

function PhysicsMotionModifier:modify(physics_component, frozen_fields, messages, dt)
    local new_x = frozen_fields.x_position
    local new_y = frozen_fields.y_position
    local new_r = frozen_fields.rotation

    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)
    dt = dt * (time_dilation_state_message ~= nil and time_dilation_state_message.factor or 1)

    for _, message in pairs(messages) do
        local instant_linear_or_dt = message.instant_linear and 1 or dt
        local instant_angular_or_dt = message.instant_angular and 1 or dt
        new_x = new_x + message.x_velocity * instant_linear_or_dt
        new_y = new_y + message.y_velocity * instant_linear_or_dt
        new_r = new_r + message.angular_velocity * instant_angular_or_dt
    end

    if physics_component.screen_wrap then
        new_x = new_x % love.graphics.getWidth()
        new_y = new_y % love.graphics.getHeight()
    end

    physics_component.body:setX(new_x)
    physics_component.body:setY(new_y)
    physics_component.body:setAngle(new_r)

    physics_component.x_position = new_x
    physics_component.y_position = new_y
    physics_component.rotation = new_r
end

return PhysicsMotionModifier