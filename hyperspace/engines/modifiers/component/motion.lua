local vector = require('lib.hump.vector-light')
local x_forward_identity, y_forward_identity = 0, 1

local MotionMessage = require('hyperspace.messages.motion')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local MultiMessageComponentModifier = require('lib.encompass').MultiMessageComponentModifier
local MotionModifier = MultiMessageComponentModifier.define(
    'MotionModifier',
    MotionMessage,
    { TimeDilationStateMessage }
)

function MotionModifier:modify(transform_component, frozen_fields, messages, dt)
    local new_x = frozen_fields.position.x
    local new_y = frozen_fields.position.y
    local new_r = frozen_fields.rotation

    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)
    dt = dt * (time_dilation_state_message ~= nil and time_dilation_state_message.factor or 1)

    for _, message in pairs(messages) do
        local instant_linear_or_dt = message.instant_linear and 1 or dt
        local instant_angular_or_dt = message.instant_angular and 1 or dt
        new_x = new_x + message.x_velocity * instant_linear_or_dt
        new_y = new_y + message.y_velocity * instant_linear_or_dt
        new_r = new_r + message.angular_velocity * instant_angular_or_dt
    end

    if frozen_fields.screen_wrap then
        new_x = new_x % love.graphics.getWidth()
        new_y = new_y % love.graphics.getHeight()
    end

    transform_component.position.x = new_x
    transform_component.position.y = new_y
    transform_component.rotation = new_r
    transform_component.forward.x, transform_component.forward.y = vector.rotate(new_r, x_forward_identity, y_forward_identity)
end

return MotionModifier