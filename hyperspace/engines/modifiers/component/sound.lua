local SoundMessage = require('hyperspace.messages.sound')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local ComponentModifier = require('lib.encompass').ComponentModifier
local SoundModifier = ComponentModifier.define(
    'SoundModifier',
    SoundMessage,
    { TimeDilationStateMessage }
)

function SoundModifier:modify(_, frozen_fields, message, dt)
    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)

    if time_dilation_state_message ~= nil then
        local source = frozen_fields.source
        source:setPitch(time_dilation_state_message.factor)
    end
end

return SoundModifier