local vector = require('lib.hump.vector-light')
local MathHelper = require('hyperspace.helpers.math')

local PhysicsAccelerationMessage = require('hyperspace.messages.physics_acceleration')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local MultiMessageComponentModifier = require('lib.encompass').MultiMessageComponentModifier
local PhysicsAccelerationModifier = MultiMessageComponentModifier.define(
    'PhysicsAccelerationModifier',
    PhysicsAccelerationMessage,
    { TimeDilationStateMessage }
)

function PhysicsAccelerationModifier:modify(physics_component, frozen_fields, messages, dt)
    local new_x_linear, new_y_linear = frozen_fields.linear_velocity.x, frozen_fields.linear_velocity.y
    local new_angular = frozen_fields.angular_velocity
    local max_linear = frozen_fields.max_linear_speed
    local max_angular = frozen_fields.max_angular_velocity

    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)
    dt = dt * (time_dilation_state_message ~= nil and time_dilation_state_message.factor or 1)

    for _, message in pairs(messages) do
        local instant_thrust_or_dt = message.instant_thrust and 1 or dt
        local instant_torque_or_dt = message.instant_torque and 1 or dt
        local x_thrust_adjusted, y_thrust_adjusted = vector.mul(instant_thrust_or_dt, message.x_thrust, message.y_thrust)
        new_x_linear, new_y_linear = vector.add(new_x_linear, new_y_linear, x_thrust_adjusted, y_thrust_adjusted)
        new_angular = new_angular + message.torque * instant_torque_or_dt
    end

    local final_x_linear, final_y_linear = vector.trim(max_linear, new_x_linear, new_y_linear)
    local final_angular_velocity = MathHelper.clamp(new_angular, -max_angular, max_angular)
    physics_component.body:setLinearVelocity(final_x_linear, final_y_linear)
    physics_component.body:setAngularVelocity(final_angular_velocity)

    physics_component.linear_velocity.x, physics_component.linear_velocity.y = final_x_linear, final_y_linear
    physics_component.angular_velocity = final_angular_velocity
end

return PhysicsAccelerationModifier