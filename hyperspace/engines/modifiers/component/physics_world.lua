local ContactComponent = require('hyperspace.components.contact')

local PhysicsWorldMessage = require('hyperspace.messages.physics_world')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local ComponentModifier = require('lib.encompass').ComponentModifier
local PhysicsWorldModifier = ComponentModifier.define(
    'PhysicsWorldModifier',
    PhysicsWorldMessage,
    { TimeDilationStateMessage }
)

function PhysicsWorldModifier:initialize()
    self.contacts_this_frame = {}

    self.collision_table_pool_size = 32
    self.inactive_collision_tables = {}
    for i = 1, 32 do
        local coll_data = {}
        coll_data.active = false
        self.inactive_collision_tables[coll_data] = coll_data
    end
    self.active_collision_tables = {}

    self.resolve_contact_function = self:create_resolve_contact_callback()
end

function PhysicsWorldModifier:get_inactive_collision_table()
    local coll_data = next(self.inactive_collision_tables)
    if coll_data == nil then
        for i = self.collision_table_pool_size, self.collision_table_pool_size * 2 do
            local empty_table = {}
            self.inactive_collision_tables[empty_table] = empty_table
        end
        self.collision_table_pool_size = self.collision_table_pool_size * 2
        coll_data = next(self.inactive_collision_tables)
    end
    self.inactive_collision_tables[coll_data] = nil
    self.active_collision_tables[coll_data] = coll_data
    coll_data.active = true
    return coll_data
end

function PhysicsWorldModifier:deactivate_collision_table(t)
    self.active_collision_tables[t] = nil
    self.inactive_collision_tables[t] = t
    t.active = false
end

local function order_entities(a, b)
    local first
    local second
    if a.id < b.id then
        first = a
        second = b
    else
        first = b
        second = a
    end

    return first, second
end

function PhysicsWorldModifier:check_contact(a, b)
    local first, second = order_entities(a, b)

    return self.contacts_this_frame[first] ~= nil
       and self.contacts_this_frame[first][second] ~= nil
end

function PhysicsWorldModifier:store_contact(a, b, coll)
    local first, second = order_entities(a, b)

    if self.contacts_this_frame[first] == nil then
        self.contacts_this_frame[first] = {}
    end

    local coll_data = self:get_inactive_collision_table()
    coll_data.x1, coll_data.y1, coll_data.x2, coll_data.y2 = coll:getPositions()
    coll_data.friction = coll:getFriction()
    coll_data.x_normal, coll_data.y_normal = coll:getNormal()
    coll_data.restitution = coll:getRestitution()

    self.contacts_this_frame[first][second] = coll_data
end

function PhysicsWorldModifier:clear_contacts()
    for first, second_table in pairs(self.contacts_this_frame) do
        for k, coll_data in pairs(second_table) do
            self:deactivate_collision_table(coll_data)
            second_table[k] = nil
        end
    end
end

function PhysicsWorldModifier:modify(physics_world_component, frozen_fields, message, dt)
    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)
    dt = dt * (time_dilation_state_message ~= nil and time_dilation_state_message.factor or 1)

    -- is there a better way to init this?
    local physics_world = physics_world_component.physics_world
    if physics_world:getCallbacks() ~= self.resolve_contact_function then
        physics_world:setCallbacks(self.resolve_contact_function)
    end

    physics_world:update(dt)

    for a_entity, b_entities in pairs(self.contacts_this_frame) do
        for b_entity, coll in pairs(b_entities) do
            if coll.active then
                local x1, y1, x2, y2 = coll.x1, coll.y1, coll.x2, coll.y2
                local friction = coll.friction
                local x_normal = coll.x_normal
                local y_normal = coll.y_normal
                local restitution = coll.restitution

                a_entity:add_component(ContactComponent,
                    'other',       b_entity,
                    'x1',          x1,
                    'y1',          y1,
                    'x2',          x2,
                    'y2',          y2,
                    'friction',    friction,
                    'x_normal',    x_normal,
                    'y_normal',    y_normal,
                    'restitution', restitution
                )

                b_entity:add_component(ContactComponent,
                    'other',       a_entity,
                    'x1',          x1,
                    'y1',          y1,
                    'x2',          x2,
                    'y2',          y2,
                    'friction',    friction,
                    'x_normal',    x_normal,
                    'y_normal',    y_normal,
                    'restitution', restitution
                )
            end
        end
    end

    self:clear_contacts()
end

function PhysicsWorldModifier:create_resolve_contact_callback()
    return function(a, b, coll)
        local a_entity = a:getUserData()
        local b_entity = b:getUserData()

        if self:check_contact(a_entity, b_entity) then return end

        self:store_contact(a_entity, b_entity, coll)
    end
end

return PhysicsWorldModifier