local TimerMessage = require('hyperspace.messages.timer')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local ComponentModifier = require('lib.encompass').ComponentModifier
local TimerModifier = ComponentModifier.define(
    'TimerModifier',
    TimerMessage,
    { TimeDilationStateMessage }
)

function TimerModifier:modify(timer_component, frozen_fields, timer_message, dt)
    local time = frozen_fields.time

    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)
    dt = dt * (time_dilation_state_message ~= nil and time_dilation_state_message.factor or 1)

    timer_component.time = time - dt
end

return TimerModifier