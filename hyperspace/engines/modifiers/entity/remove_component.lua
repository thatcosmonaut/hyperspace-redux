local RemoveComponentMessage = require('hyperspace.messages.remove_component')

local EntityModifier = require('lib.encompass').EntityModifier
local RemoveComponentModifier = EntityModifier.define('RemoveComponentModifier', RemoveComponentMessage)

function RemoveComponentModifier:modify(entity, messages, dt)
    for _, message in pairs(messages) do
        entity:remove_component(message.component_to_remove)
    end
end

return RemoveComponentModifier