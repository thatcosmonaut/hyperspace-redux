local DeactivateMessage = require('hyperspace.messages.deactivate')

local EntityModifier = require('lib.encompass').EntityModifier
local DeactivateModifier = EntityModifier.define('DeactivateModifier', DeactivateMessage)

function DeactivateModifier:modify(entity, messages, dt)
    entity:deactivate()
end

return DeactivateModifier