local AddComponentMessage = require('hyperspace.messages.add_component')

local EntityModifier = require('lib.encompass').EntityModifier
local AddComponentModifier = EntityModifier.define('AddComponentModifier', AddComponentMessage)

function AddComponentModifier:modify(entity, messages, dt)
    for _, message in pairs(messages) do
        entity:add_component(message.component_to_add, unpack(message.component_args))
    end
end

return AddComponentModifier