local DestroyMessage = require('hyperspace.messages.destroy')

local EntityModifier = require('lib.encompass').EntityModifier
local DestroyModifier = EntityModifier.define('DestroyModifier', DestroyMessage)

function DestroyModifier:modify(entity, messages, dt)
    entity:destroy()
end

return DestroyModifier