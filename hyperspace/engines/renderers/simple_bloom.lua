local SimpleBloomComponent = require('hyperspace.components.draw.effects.simple_bloom')

local Renderer = require('lib.encompass').Renderer
local SimpleBloomRenderer = Renderer.define('SimpleBloomRenderer', { SimpleBloomComponent })

function SimpleBloomRenderer:initialize()
    self.canvas = love.graphics.newCanvas()
end

function SimpleBloomRenderer:render(entity, canvas)
    local simple_bloom_component = entity:get_component(SimpleBloomComponent)

    love.graphics.setBlendMode('alpha', 'premultiplied')
    love.graphics.setColor(1, 1, 1, 1)

    love.graphics.setCanvas(self.canvas)
    love.graphics.clear()
    love.graphics.setShader(simple_bloom_component.shader)
    love.graphics.draw(canvas)
    love.graphics.setShader()

    love.graphics.setCanvas(canvas)
    love.graphics.clear()
    love.graphics.draw(self.canvas)
end

return SimpleBloomRenderer