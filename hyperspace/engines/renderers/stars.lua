local StarsComponent = require('hyperspace.components.draw.stars')

local Renderer = require('lib.encompass').Renderer
local StarsRenderer = Renderer.define('StarsRenderer', { StarsComponent })

function StarsRenderer:render(entity, canvas)
    love.graphics.setCanvas(canvas)
    local stars_component = entity:get_component(StarsComponent)

    local stars = stars_component.stars

    for _, star in pairs(stars) do
        local r, g, b, a = love.graphics.getColor()
        love.graphics.setColor(star.r, star.g, star.b, star.a)
        love.graphics.ellipse('fill', star.x, star.y, 2, 2)
        love.graphics.setColor(r, g, b, a)
    end
    love.graphics.setCanvas()
end

return StarsRenderer