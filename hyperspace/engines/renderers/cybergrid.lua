local PlayMat = require('lib.playmat')

local CybergridComponent = require('hyperspace.components.draw.cybergrid')

local Renderer = require('lib.encompass').Renderer
local CybergridRenderer = Renderer.define('CybergridRenderer', { CybergridComponent })

function CybergridRenderer:render(entity, canvas)
    local cybergrid_component = entity:get_component(CybergridComponent)

    love.graphics.setCanvas(canvas)
    love.graphics.setColor(70 / 255, 173 / 255, 212 / 255, 1)
    love.graphics.setBlendMode('alpha')

    for k, v in ipairs(cybergrid_component.horizontal_lines) do
        local start_x, start_y, _ = PlayMat.toScreen(cybergrid_component.camera, v.x_start, v.y_start)
        local end_x, end_y, _ = PlayMat.toScreen(cybergrid_component.camera, v.x_end, v.y_end)

        love.graphics.line(start_x, start_y, end_x, end_y)
    end

    for k, v in ipairs(cybergrid_component.vertical_lines) do
        local start_x, start_y, _ = PlayMat.toScreen(cybergrid_component.camera, v.x_start, v.y_start)
        local end_x, end_y, _ = PlayMat.toScreen(cybergrid_component.camera, v.x_end, v.y_end)

        love.graphics.line(start_x, start_y, end_x, end_y)
    end

    love.graphics.setCanvas()
end

return CybergridRenderer