local ShockwaveDrawComponent = require('hyperspace.components.draw.effects.shockwave')

local Renderer = require('lib.encompass').Renderer
local ShockwaveRenderer = Renderer.define('ShockwaveRenderer', { ShockwaveDrawComponent })

function ShockwaveRenderer:initialize()
    self.canvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())
end

function ShockwaveRenderer:render(entity, canvas)
    local shockwave_draw_component = entity:get_component(ShockwaveDrawComponent)

    love.graphics.setBlendMode('alpha', 'premultiplied')

    love.graphics.setCanvas(self.canvas)
    love.graphics.clear()
    love.graphics.setShader(shockwave_draw_component.shader)
    love.graphics.draw(canvas)
    love.graphics.setShader()

    love.graphics.setCanvas(canvas)
    love.graphics.clear()
    love.graphics.draw(self.canvas)
end

return ShockwaveRenderer