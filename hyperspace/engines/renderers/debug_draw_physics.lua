local PhysicsComponent = require('hyperspace.components.physics')
local DebugDrawPhysicsComponent = require('hyperspace.components.draw.debug_draw_physics')

local Renderer = require('lib.encompass').Renderer
local DebugDrawPhysicsRenderer = Renderer.define(
    'DebugDrawPhysicsRenderer',
    { PhysicsComponent, DebugDrawPhysicsComponent }
)

function DebugDrawPhysicsRenderer:render(entity, canvas)
    love.graphics.setCanvas(canvas)
    local collision_component = entity:get_component(PhysicsComponent)
    local debug_draw_physics_component = entity:get_component(DebugDrawPhysicsComponent)

    local collision_body = collision_component.body
    --debug_draw_physics_component:draw(collision_body)

    love.graphics.setColor(0, 0, 0, 1)
    for _, fixture in pairs(collision_body:getFixtures()) do
        love.graphics.polygon('fill', collision_body:getWorldPoints(fixture:getShape():getPoints()))
    end
    love.graphics.setColor(1, 1, 1, 1)
    for _, fixture in pairs(collision_body:getFixtures()) do
        love.graphics.polygon('line', collision_body:getWorldPoints(fixture:getShape():getPoints()))
    end
    love.graphics.setCanvas()
end

return DebugDrawPhysicsRenderer