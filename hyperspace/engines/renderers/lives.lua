local LivesComponent = require('hyperspace.components.draw.lives')

local Renderer = require('lib.encompass').Renderer
local LivesRenderer = Renderer.define('LivesRenderer', { LivesComponent })

function LivesRenderer:initialize()
    self.life_canvas = love.graphics.newCanvas(32, 32, {format = 'normal', msaa = 4})

    love.graphics.setCanvas(self.life_canvas)
    love.graphics.clear()
    love.graphics.setLineJoin('bevel')
    love.graphics.setBlendMode('alpha')
    love.graphics.setColor(0, 0, 0, 1)
    love.graphics.polygon('fill', 16-6, 16-6, 16+0, 16+12, 16+6, 16-6, 16-6, 16-6)
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.line(16-6, 16-6, 16+0, 16+12, 16+6, 16-6, 16-6, 16-6)
    love.graphics.setCanvas()
end

function LivesRenderer:render(entity, canvas)
    local lives_component = entity:get_component(LivesComponent)

    local count = lives_component.count

    love.graphics.setCanvas(canvas)
    local x_start = 8
    local x_increment = 16
    for i = 1, count do
        love.graphics.draw(self.life_canvas, x_start + x_increment * i, 48, math.pi, 1, 1, 16, 16)
    end
end

return LivesRenderer