local TransformComponent = require("hyperspace.components.transform")
local DrawCanvasComponent = require('hyperspace.components.draw.canvas')

local Renderer = require('lib.encompass').Renderer
local CanvasRenderer = Renderer.define("CanvasRenderer", { TransformComponent, DrawCanvasComponent })

function CanvasRenderer:render(entity, canvas)
    love.graphics.setCanvas(canvas)
    local transform_component = entity:get_component(TransformComponent)
    local draw_canvas_component = entity:get_component(DrawCanvasComponent)

    local x = transform_component.position.x
    local y = transform_component.position.y
    local r = transform_component.rotation

    local component_canvas = draw_canvas_component.canvas
    local w = draw_canvas_component.w
    local h = draw_canvas_component.h

    local mode, alphamode = love.graphics.getBlendMode()
    love.graphics.setBlendMode('alpha', 'premultiplied')
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(component_canvas, x, y, r, 1, 1, w * 0.5, h * 0.5)
    love.graphics.setBlendMode(mode, alphamode)
    love.graphics.setCanvas()
end

return CanvasRenderer