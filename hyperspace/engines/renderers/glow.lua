--[[
Public domain:
Copyright (C) 2017 by Matthias Richter <vrld@vrld.org>
Modified in 2018 by Evan Hemsley <evan@moonside.games>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.
THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
]]--

local GlowDrawComponent = require('hyperspace.components.draw.effects.glow')

local Renderer = require('lib.encompass').Renderer
local GlowRenderer = Renderer.define('GlowRenderer', { GlowDrawComponent })

function GlowRenderer:initialize()
    self.front = love.graphics.newCanvas(
        love.graphics.getWidth(),
        love.graphics.getHeight(),
        {
            msaa = 2
        }
    )
    self.back = love.graphics.newCanvas(
        love.graphics.getWidth(),
        love.graphics.getHeight(),
        {
            msaa = 2
        }
    )
end

function GlowRenderer:render(entity, canvas)
    local glow_draw_component = entity:get_component(GlowDrawComponent)

    love.graphics.setBlendMode('alpha', 'premultiplied')
    love.graphics.setColor(1, 1, 1, 1)

     -- 1st pass: draw scene with brightness threshold
    love.graphics.setCanvas(self.front)
    love.graphics.clear()
    love.graphics.setShader(glow_draw_component.threshold_shader)
    love.graphics.draw(canvas)

     -- 2nd pass: apply blur shader in x
    love.graphics.setCanvas(self.back)
    love.graphics.clear()
    love.graphics.setShader(glow_draw_component.first_blur_shader)
    love.graphics.draw(self.front)

     -- 3nd pass: apply blur shader in y and draw original and blurred scene
    love.graphics.setCanvas(self.front)
    love.graphics.clear()

     -- original scene without blur shader
    love.graphics.setShader()
    love.graphics.setBlendMode('add', 'premultiplied')
    love.graphics.draw(canvas)

     -- second pass of light blurring
    love.graphics.setShader(glow_draw_component.second_blur_shader)
    love.graphics.draw(self.back)

    love.graphics.setCanvas(canvas)
    love.graphics.setBlendMode('add', 'premultiplied')
    love.graphics.draw(self.front)

    love.graphics.setCanvas()
end

return GlowRenderer