local ScanlinesDrawComponent = require('hyperspace.components.draw.effects.scanlines')

local Renderer = require('lib.encompass').Renderer
local ScanlinesRenderer = Renderer.define('ScanlinesRenderer', { ScanlinesDrawComponent })

function ScanlinesRenderer:initialize()
    self.canvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())
end

function ScanlinesRenderer:render(entity, canvas)
    local scanlines_draw_component = entity:get_component(ScanlinesDrawComponent)

    love.graphics.setBlendMode('alpha', 'premultiplied')
    love.graphics.setColor(1, 1, 1, 1)

    love.graphics.setCanvas(self.canvas)
    love.graphics.clear()
    love.graphics.setShader(scanlines_draw_component.shader)
    love.graphics.draw(canvas)
    love.graphics.setShader()

    love.graphics.setCanvas(canvas)
    love.graphics.clear()
    love.graphics.draw(self.canvas)
end

return ScanlinesRenderer