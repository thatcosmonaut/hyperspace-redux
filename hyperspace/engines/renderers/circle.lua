local TransformComponent = require("hyperspace.components.transform")
local DrawCircleComponent = require('hyperspace.components.draw.circle')

local Renderer = require('lib.encompass').Renderer
local CircleRenderer = Renderer.define("CircleRenderer", { TransformComponent, DrawCircleComponent })

function CircleRenderer:render(entity, canvas)
    love.graphics.setCanvas(canvas)
    local transform_component = entity:get_component(TransformComponent)
    local draw_circle_component = entity:get_component(DrawCircleComponent)

    love.graphics.circle('fill', transform_component.position.x, transform_component.position.y, draw_circle_component.radius)
    love.graphics.setCanvas()
end

return CircleRenderer