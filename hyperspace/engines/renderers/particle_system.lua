local ParticleSystemDrawComponent = require('hyperspace.components.draw.particle_system')

local Renderer = require('lib.encompass').Renderer
local ParticleSystemRenderer = Renderer.define('ParticleSystemRenderer', { ParticleSystemDrawComponent })

function ParticleSystemRenderer:render(entity, canvas)
    local particle_system_draw_component = entity:get_component(ParticleSystemDrawComponent)
    local particle_system = particle_system_draw_component.particle_system

    local mode, alphamode = love.graphics.getBlendMode()
    love.graphics.setCanvas(canvas)
    love.graphics.setBlendMode('add')
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(particle_system)
    love.graphics.setBlendMode(mode, alphamode)
end

return ParticleSystemRenderer