local ParticleSystemSideEffectMessage = require('hyperspace.messages.particle_system')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

local SideEffecter = require('lib.encompass').SideEffecter
local ParticleSystemSideEffecter = SideEffecter.define(
    'ParticleSystemSideEffecter',
    ParticleSystemSideEffectMessage,
    { TimeDilationStateMessage }
)

function ParticleSystemSideEffecter:effect(message, dt)
    local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)
    dt = dt * (time_dilation_state_message ~= nil and time_dilation_state_message.factor or 1)

    message.particle_system:update(dt)
end

return ParticleSystemSideEffecter