local SwitchGamestateMessage = require('hyperspace.messages.switch_gamestate')

local SideEffecter = require('lib.encompass').SideEffecter
local SwitchGamestateEngine = SideEffecter.define('SwitchGamestateEngine', SwitchGamestateMessage)

function SwitchGamestateEngine:initialize(hyperspace)
    self.Hyperspace = hyperspace
end

function SwitchGamestateEngine:effect(message)
    self.Hyperspace.switch_gamestate(message.gamestate)
end

return SwitchGamestateEngine