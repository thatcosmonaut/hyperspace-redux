local ParticleEmissionMessage = require('hyperspace.messages.particle_emission')

local SideEffecter = require('lib.encompass').SideEffecter
local ParticleEmissionSideEffecter = SideEffecter.define('ParticleEmissionSideEffecter', ParticleEmissionMessage)

function ParticleEmissionSideEffecter:effect(message, dt)
    message.particle_system:moveTo(message.x, message.y)
    message.particle_system:emit(message.amount)
end

return ParticleEmissionSideEffecter