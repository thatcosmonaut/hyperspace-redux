local ParticleSystemDrawComponent = require('hyperspace.components.draw.particle_system')
local ParticleSystemMessage = require('hyperspace.messages.particle_system')

local Detecter = require('lib.encompass').Detecter
local ParticleSystemDetecter = Detecter.define('ParticleSystemDetecter', { ParticleSystemDrawComponent })

function ParticleSystemDetecter:detect(entity)
    local particle_system_draw_component = entity:get_component(ParticleSystemDrawComponent)

    self:create_message(ParticleSystemMessage,
        'particle_system', particle_system_draw_component.particle_system
    )
end

return ParticleSystemDetecter