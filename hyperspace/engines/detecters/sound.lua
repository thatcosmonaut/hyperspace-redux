
local SoundComponent = require('hyperspace.components.sound')
local SoundMessage = require('hyperspace.messages.sound')
local DestroyMessage = require('hyperspace.messages.destroy')

local Detecter = require('lib.encompass').Detecter
local SoundEffectDetecter = Detecter.define('SoundEffectDetecter', { SoundComponent })

function SoundEffectDetecter:detect(entity)
    local sound_component = entity:get_component(SoundComponent)

    if not sound_component.source:isPlaying() then
        self:create_message(DestroyMessage,
            'entity', entity
        )
    else
        self:create_message(SoundMessage,
            'component', sound_component
        )
    end
end

return SoundEffectDetecter