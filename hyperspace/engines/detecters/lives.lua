local LivesComponent = require('hyperspace.components.draw.lives')

local GameOverSpawnMessage = require('hyperspace.messages.spawners.game_over')
local RemoveComponentEntityMessage = require('hyperspace.messages.remove_component')

local Detecter = require('lib.encompass').Detecter
local LivesDetecter = Detecter.define('LivesDetecter', { LivesComponent })

function LivesDetecter:detect(entity)
    local lives_component = entity:get_component(LivesComponent)
    local count = lives_component.count

    if count <= 0 then
        self:create_message(RemoveComponentEntityMessage,
            'entity', entity,
            'component_to_remove', lives_component
        )

        self:create_message(GameOverSpawnMessage, 'time', 5)
    end
end

return LivesDetecter