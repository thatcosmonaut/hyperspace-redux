
local ShockwaveDrawComponent = require('hyperspace.components.draw.effects.shockwave')
local ShockwaveMessage = require('hyperspace.messages.shockwave')

local PassthroughDetecter = require('hyperspace.engines.detecters.meta.passthrough')

return PassthroughDetecter('ShockwaveDetecter', ShockwaveDrawComponent, ShockwaveMessage)