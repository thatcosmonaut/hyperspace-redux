local Detecter = require('lib.encompass').Detecter

local ContactComponent = require('hyperspace.components.contact')

return function(name, CollisionResponseComponent, ...)
    local other_component_types = {...}

    local CollisionResponseDetecter = Detecter.define(
        name,
        { CollisionResponseComponent, ContactComponent, unpack(other_component_types) }
    )

    function CollisionResponseDetecter:detect(entity)
        local collision_response_components = entity:get_components(CollisionResponseComponent)
        local contact_component = entity:get_component(ContactComponent)
        local other_components = {}
        for _, component_type in pairs(other_component_types) do
            table.insert(other_components, entity:get_components(component_type))
        end

        for _, collision_response_component in pairs(collision_response_components) do
            for _, collision_type in pairs(collision_response_component.collision_types) do
                if contact_component.other:has_component(collision_type) then
                    self:respond(entity, collision_response_component, contact_component, unpack(other_components))
                end
            end
        end
    end

    return CollisionResponseDetecter
end