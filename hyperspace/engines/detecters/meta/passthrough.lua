local Detecter = require('lib.encompass').Detecter

return function(name, component_type, message_type)
    local PassthroughDetecter = Detecter.define(name, { component_type })

    function PassthroughDetecter:detect(entity)
        self:create_message(message_type, 'component', entity:get_component(component_type))
    end

    return PassthroughDetecter
end