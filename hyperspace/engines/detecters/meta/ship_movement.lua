local thrust_value = 600
local rotation_speed = math.pi * 2

local Easing = require('lib.easing')
local vector = require('lib.hump.vector-light')

local TransformComponent = require('hyperspace.components.transform')
local VelocityComponent = require('hyperspace.components.velocity')
local AccelerationMessage = require('hyperspace.messages.acceleration')

local Detecter = require('lib.encompass').Detecter

return function(InputComponent, axis_input_x_function, axis_input_y_function)
    local ShipMovementGamepadControlDetecter = Detecter.define(
        'ShipMovementGamepadControlDetecter',
        {
            TransformComponent,
            VelocityComponent,
            InputComponent
        }
    )

    function ShipMovementGamepadControlDetecter:detect(entity)
        local transform_component = entity:get_component(TransformComponent)
        local velocity_component = entity:get_component(VelocityComponent)

        local axis_input_x = axis_input_x_function(entity)
        local axis_input_y = axis_input_y_function(entity)

        local x_thrust, y_thrust = 0, 0
        if axis_input_y < 0 then
            local scalar = axis_input_y_function(entity) * -1 * thrust_value
            x_thrust, y_thrust = vector.mul(scalar, transform_component.forward.x, transform_component.forward.y)
        end

        local torque = (Easing.linear(axis_input_x, 0, rotation_speed, 1) - velocity_component.angular)

        self:create_message(AccelerationMessage,
            'component',      velocity_component,
            'x_thrust',       x_thrust,
            'y_thrust',       y_thrust,
            'torque',         torque,
            'instant_thrust', false,
            'instant_torque', true
        )
    end

    return ShipMovementGamepadControlDetecter
end


