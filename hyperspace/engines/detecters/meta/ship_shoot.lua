local bullet_speed = 900

local vector = require('lib.hump.vector-light')

local TransformComponent = require('hyperspace.components.transform')
local GunComponent = require('hyperspace.components.gun')
local CanShootComponent = require('hyperspace.components.can_shoot')
local AddComponentTimerComponent = require('hyperspace.components.add_component_timer')

local BulletSpawnMessage = require('hyperspace.messages.spawners.bullet')
local SoundSpawnMessage = require('hyperspace.messages.spawners.sound')
local AddComponentMessage = require('hyperspace.messages.add_component')
local RemoveComponentMessage = require('hyperspace.messages.remove_component')
local Detecter = require('lib.encompass').Detecter

return function(InputComponent, fire_function)
    local ShipShootControlDetecter = Detecter.define(
        'ShipShootControlDetecter',
        { InputComponent, GunComponent, TransformComponent }
    )

    function ShipShootControlDetecter:detect(entity)
        local transform_component = entity:get_component(TransformComponent)
        local gun_component = entity:get_component(GunComponent)

        local fire = fire_function(entity)

        if fire and entity:has_component(CanShootComponent) then
            local can_shoot_component = entity:get_component(CanShootComponent)

            local x_pos, y_pos = vector.add(transform_component.position.x, transform_component.position.y, vector.mul(12, transform_component.forward.x, transform_component.forward.y))
            local x_vel, y_vel = vector.mul(bullet_speed, transform_component.forward.x, transform_component.forward.y)

            self:create_message(BulletSpawnMessage,
                'x_position', x_pos,
                'y_position', y_pos,
                'x_velocity', x_vel,
                'y_velocity', y_vel
            )

            self:create_message(SoundSpawnMessage,
                'source', love.audio.newSource('hyperspace/assets/sounds/bullet.wav', 'static')
            )

            self:create_message(AddComponentMessage,
                'entity',               entity,
                'component_to_add',     AddComponentTimerComponent,
                'component_args', {
                    'time',             gun_component.interval,
                    'component_to_add', CanShootComponent,
                    'component_args',   {}
                }
            )

            self:create_message(RemoveComponentMessage,
                'entity', entity,
                'component_to_remove', can_shoot_component
            )
        end
    end

    return ShipShootControlDetecter
end