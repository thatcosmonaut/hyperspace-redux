local KeyboardInputComponent = require('hyperspace.components.keyboard_input')

local ShipMovementMeta = require('hyperspace.engines.detecters.meta.ship_movement')

local function bool_to_number(bool)
    return bool and 1 or 0
end

local function axis_input_x_function(_)
    local axis_input_x = 0

    axis_input_x = axis_input_x - bool_to_number(love.keyboard.isDown('left'))
    axis_input_x = axis_input_x + bool_to_number(love.keyboard.isDown('right'))

    return axis_input_x
end

local function axis_input_y_function(_)
    local axis_input_y = 0

    axis_input_y = axis_input_y - bool_to_number(love.keyboard.isDown('up'))
    axis_input_y = axis_input_y + bool_to_number(love.keyboard.isDown('down'))

    return axis_input_y
end

return ShipMovementMeta(KeyboardInputComponent, axis_input_x_function, axis_input_y_function)