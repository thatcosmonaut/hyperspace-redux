local Easing = require('lib.easing')

local TimeDilationComponent = require('hyperspace.components.time_dilation')

local TimeDilationMessage = require('hyperspace.messages.time_dilation')
local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')
local RemoveComponentMessage = require('hyperspace.messages.remove_component')

local Detecter = require('lib.encompass').Detecter
local TimeDilationDetecter = Detecter.define('TimeDilationDetecter', { TimeDilationComponent })

function TimeDilationDetecter:detect(entity)
    local time_dilation_component = entity:get_component(TimeDilationComponent)

    local factor = time_dilation_component.factor
    local time_elapsed = time_dilation_component.time_elapsed
    local ease_in_time = time_dilation_component.ease_in_time
    local time = time_dilation_component.time
    local ease_out_time = time_dilation_component.ease_out_time

    self:create_message(TimeDilationMessage,
        'component', time_dilation_component
    )

    local calculated_factor = 1

    if time_elapsed < ease_in_time then
        calculated_factor = Easing.inQuad(time_elapsed, 1, factor - 1, ease_in_time)
    elseif time_elapsed < ease_in_time + time then
        calculated_factor = factor
    elseif time_elapsed < ease_in_time + time + ease_out_time then
        local elapsed_out_time = time_elapsed - ease_in_time - time
        calculated_factor = Easing.inQuint(elapsed_out_time, factor, 1 - factor, ease_out_time)
    else
        self:create_message(RemoveComponentMessage,
            'entity',              entity,
            'component_to_remove', time_dilation_component
        )
    end

    self:create_message(TimeDilationStateMessage,
        'factor', calculated_factor
    )
end

return TimeDilationDetecter