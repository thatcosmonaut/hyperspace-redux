local KeyboardInputComponent = require('hyperspace.components.keyboard_input')
local TransformComponent = require('hyperspace.components.transform')
local CanBlinkComponent = require('hyperspace.components.can_blink')
local AddComponentTimerComponent = require('hyperspace.components.add_component_timer')

local AddComponentMessage = require('hyperspace.messages.add_component')
local MotionMessage = require('hyperspace.messages.motion')
local RemoveComponentMessage = require('hyperspace.messages.remove_component')

local Detecter = require('lib.encompass').Detecter
local ShipBlinkDetecter = Detecter.define('ShipBlinkDetecter', {
    KeyboardInputComponent
})

function ShipBlinkDetecter:detect(entity)
    local transform_component = entity:get_component(TransformComponent)

    local blink = love.keyboard.isDown('a')

    if blink and entity:has_component(CanBlinkComponent) then
        local can_blink_component = entity:get_component(CanBlinkComponent)

        local motion_vector = transform_component.forward:normalized() * 150

        self:create_message(MotionMessage,
            'component',        transform_component,
            'x_velocity',       motion_vector.x,
            'y_velocity',       motion_vector.y,
            'angular_velocity', 0,
            'instant_linear',   true,
            'instant_angular',  false
        )

        self:create_message(AddComponentMessage,
            'entity',               entity,
            'component_to_add',     AddComponentTimerComponent,
            'component_args', {
                'time',             2,
                'component_to_add', CanBlinkComponent,
                'component_args',   {}
            }
        )

        self:create_message(RemoveComponentMessage,
            'entity',              entity,
            'component_to_remove', can_blink_component
        )
    end
end

return ShipBlinkDetecter