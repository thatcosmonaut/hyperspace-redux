local CybergridComponent = require('hyperspace.components.draw.cybergrid')
local CybergridMessage = require('hyperspace.messages.cybergrid')

local PassthroughDetecter = require('hyperspace.engines.detecters.meta.passthrough')

return PassthroughDetecter('CybergridDetecter', CybergridComponent, CybergridMessage)