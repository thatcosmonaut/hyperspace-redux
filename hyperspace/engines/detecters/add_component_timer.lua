local AddComponentTimerComponent = require('hyperspace.components.add_component_timer')
local TimerMessage = require('hyperspace.messages.timer')
local AddComponentMessage = require('hyperspace.messages.add_component')
local RemoveComponentMessage = require('hyperspace.messages.remove_component')

local Detecter = require('lib.encompass').Detecter
local AddComponentTimerDetecter = Detecter.define('AddComponentTimerDetecter', { AddComponentTimerComponent })

function AddComponentTimerDetecter:detect(entity)
    local timer_components = entity:get_components(AddComponentTimerComponent)

    for _, timer_component in pairs(timer_components) do
        if timer_component.time <= 0 then
            self:create_message(RemoveComponentMessage,
                'entity',              entity,
                'component_to_remove', timer_component
            )
            self:create_message(AddComponentMessage,
                'entity',           entity,
                'component_to_add', timer_component.component_to_add,
                'component_args',   timer_component.component_args
            )
        else
            self:create_message(TimerMessage,
                'component', timer_component
            )
        end
    end
end

return AddComponentTimerDetecter