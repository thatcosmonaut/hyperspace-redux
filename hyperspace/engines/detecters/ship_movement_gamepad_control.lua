local GamepadInputComponent = require('hyperspace.components.gamepad_input')

local ShipMovementMeta = require('hyperspace.engines.detecters.meta.ship_movement')

local function axis_input_x_function(entity)
    local gamepad_input_component = entity:get_component(GamepadInputComponent)
    local joystick = love.joystick.getJoysticks()[gamepad_input_component.index]

    return joystick:getGamepadAxis('leftx')
end

local function axis_input_y_function(entity)
    local gamepad_input_component = entity:get_component(GamepadInputComponent)
    local joystick = love.joystick.getJoysticks()[gamepad_input_component.index]

    return joystick:getGamepadAxis('triggerright') * -1
end

return ShipMovementMeta(GamepadInputComponent, axis_input_x_function, axis_input_y_function)