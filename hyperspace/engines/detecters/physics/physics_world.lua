local PhysicsComponent = require('hyperspace.components.physics_world')
local PhysicsWorldMessage = require('hyperspace.messages.physics_world')

local Detecter = require('lib.encompass').Detecter
local PhysicsWorldDetecter = Detecter.define('PhysicsWorldDetecter', { PhysicsComponent })

function PhysicsWorldDetecter:detect(entity)
    local physics_world_component = entity:get_component(PhysicsComponent)

    self:create_message(PhysicsWorldMessage,
        'component', physics_world_component
    )
end

return PhysicsWorldDetecter