local vector = require('lib.hump.vector-light')

local PhysicsComponent = require('hyperspace.components.physics')
local VelocityComponent = require('hyperspace.components.velocity')
local UpdateFromPhysicsComponent = require('hyperspace.components.update_from_physics')
local AccelerationMessage = require('hyperspace.messages.acceleration')

local Detecter = require('lib.encompass').Detecter
local UpdateVelocityFromPhysicsDetecter = Detecter.define(
    'UpdateVelocityFromPhysicsDetecter',
    { UpdateFromPhysicsComponent, PhysicsComponent, VelocityComponent }
)

function UpdateVelocityFromPhysicsDetecter:detect(entity)
    local collision_component = entity:get_component(PhysicsComponent)
    local velocity_component = entity:get_component(VelocityComponent)

    local body_x_linear, body_y_linear = collision_component.body:getLinearVelocity()
    local dx, dy = vector.sub(body_x_linear, body_y_linear, velocity_component.linear.x, velocity_component.linear.y)
    local angular_delta = collision_component.body:getAngularVelocity() - velocity_component.angular

    self:create_message(AccelerationMessage,
        'component', velocity_component,
        'x_thrust', dx,
        'y_thrust', dy,
        'torque', angular_delta,
        'instant_thrust', true,
        'instant_torque', true
    )
end

return UpdateVelocityFromPhysicsDetecter