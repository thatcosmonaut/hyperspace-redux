local vector = require('lib.hump.vector-light')

local PhysicsComponent = require('hyperspace.components.physics')
local VelocityComponent = require('hyperspace.components.velocity')
local UpdateFromPositionAndVelocityComponent = require(
    'hyperspace.components.update_from_position_and_velocity'
)
local PhysicsAccelerationMessage = require('hyperspace.messages.physics_acceleration')

local Detecter = require('lib.encompass').Detecter
local UpdatePhysicsVelocityDetecter = Detecter.define(
    'UpdatePhysicsVelocityDetecter',
    {
        UpdateFromPositionAndVelocityComponent,
        PhysicsComponent,
        VelocityComponent
    }
)

function UpdatePhysicsVelocityDetecter:detect(entity)
    local collision_component = entity:get_component(PhysicsComponent)
    local velocity_component = entity:get_component(VelocityComponent)

    local linear = collision_component.linear_velocity
    local angular = collision_component.angular_velocity

    local dx, dy = vector.sub(velocity_component.linear.x, velocity_component.linear.y, linear.x, linear.y)
    local angular_delta = velocity_component.angular - angular

    self:create_message(PhysicsAccelerationMessage,
        'component',      collision_component,
        'x_thrust',       dx,
        'y_thrust',       dy,
        'torque',         angular_delta,
        'instant_thrust', true,
        'instant_torque', true
    )
end

return UpdatePhysicsVelocityDetecter