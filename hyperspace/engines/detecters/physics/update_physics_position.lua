
local PhysicsComponent = require('hyperspace.components.physics')
local TransformComponent = require('hyperspace.components.transform')
local UpdateFromPositionAndVelocityComponent = require(
    'hyperspace.components.update_from_position_and_velocity'
)
local PhysicsMotionMessage = require('hyperspace.messages.physics_motion')

local Detecter = require('lib.encompass').Detecter
local UpdatePhysicsPositionDetecter = Detecter.define(
    'UpdatePhysicsPositionDetecter',
    {
        UpdateFromPositionAndVelocityComponent,
        PhysicsComponent,
        TransformComponent
    }
)

function UpdatePhysicsPositionDetecter:detect(entity)
    local collision_component = entity:get_component(PhysicsComponent)
    local transform_component = entity:get_component(TransformComponent)

    local x_delta = transform_component.position.x - collision_component.x_position
    local y_delta = transform_component.position.y - collision_component.y_position
    local rotation_delta = transform_component.rotation - collision_component.rotation

    self:create_message(PhysicsMotionMessage,
        'component', collision_component,
        'x_velocity', x_delta,
        'y_velocity', y_delta,
        'angular_velocity', rotation_delta,
        'instant_linear', true,
        'instant_angular', true
    )
end

return UpdatePhysicsPositionDetecter