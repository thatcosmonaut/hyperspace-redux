local vector = require('lib.hump.vector-light')

local PhysicsComponent = require('hyperspace.components.physics')
local TransformComponent = require('hyperspace.components.transform')
local UpdateFromPhysicsComponent = require('hyperspace.components.update_from_physics')
local MotionMessage = require('hyperspace.messages.motion')

local Detecter = require('lib.encompass').Detecter
local UpdateTransformFromPhysicsDetecter = Detecter.define(
    'UpdateTransformFromPhysicsDetecter',
    { UpdateFromPhysicsComponent, PhysicsComponent, TransformComponent }
)

function UpdateTransformFromPhysicsDetecter:detect(entity)
    local collision_component = entity:get_component(PhysicsComponent)
    local transform_component = entity:get_component(TransformComponent)

    local dx, dy = vector.sub(collision_component.body:getX(), collision_component.body:getY(), transform_component.position.x, transform_component.position.y)
    local rotation_delta = collision_component.body:getAngle() - transform_component.rotation

    self:create_message(MotionMessage,
        'component',        transform_component,
        'x_velocity',       dx,
        'y_velocity',       dy,
        'angular_velocity', rotation_delta,
        'instant_linear',   true,
        'instant_angular',  true
    )
end

return UpdateTransformFromPhysicsDetecter