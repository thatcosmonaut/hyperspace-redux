local MathHelper = require('hyperspace.helpers.math')

local MusicComponent = require('hyperspace.components.music')
local SimpleBloomComponent = require('hyperspace.components.draw.effects.simple_bloom')

local SimpleBloomMessage = require('hyperspace.messages.simple_bloom')

local Detecter = require('lib.encompass').Detecter
local BeatBloomDetecter = Detecter.define('BeatBloomDetecter', {
    MusicComponent, SimpleBloomComponent
})

function BeatBloomDetecter:get_intensity(music_component)
    for _, section in ipairs(music_component.sections) do
        if section.time == nil then
            return section.intensity
        elseif music_component.time < section.time then
            return section.intensity
        end
    end
end

function BeatBloomDetecter:detect(entity)
    local music_component = entity:get_component(MusicComponent)
    local simple_bloom_component = entity:get_component(SimpleBloomComponent)
    local intensity = self:get_intensity(music_component)

    local power = 0.5 + intensity * math.sin((2 * math.pi) / (1 / music_component.bpm) / 60 * music_component.time)
    power = MathHelper.clamp(power, 0.5, 2.0)

    self:create_message(SimpleBloomMessage,
        'component', simple_bloom_component,
        'power',     power
    )
end

return BeatBloomDetecter