local KeyboardInputComponent = require('hyperspace.components.keyboard_input')

local ShipShootMeta = require('hyperspace.engines.detecters.meta.ship_shoot')

local function fire_function(_)
    return love.keyboard.isDown('z')
end

return ShipShootMeta(KeyboardInputComponent, fire_function)