local TransformComponent = require("hyperspace.components.transform")
local VelocityComponent = require("hyperspace.components.velocity")
local MotionMessage = require("hyperspace.messages.motion")

local Detecter = require("lib.encompass").Detecter
local MovementDetecter = Detecter.define(
    'MovementDetecter',
    {TransformComponent, VelocityComponent}
)

function MovementDetecter:detect(entity)
    local transform_component = entity:get_component(TransformComponent)
    local velocity_component = entity:get_component(VelocityComponent)

    self:create_message(MotionMessage,
        'component', transform_component,
        'x_velocity', velocity_component.linear.x,
        'y_velocity', velocity_component.linear.y,
        'angular_velocity', velocity_component.angular,
        'instant_linear', false,
        'instant_angular', false
    )
end

return MovementDetecter
