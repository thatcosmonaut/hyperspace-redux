local GamepadInputComponent = require('hyperspace.components.gamepad_input')

local ShipShootMeta = require('hyperspace.engines.detecters.meta.ship_shoot')

local function fire_function(entity)
    local gamepad_input_component = entity:get_component(GamepadInputComponent)
    local joystick = love.joystick.getJoysticks()[gamepad_input_component.index]
    return joystick:isGamepadDown('a')
end

return ShipShootMeta(GamepadInputComponent, fire_function)