local ShockwaveCollisionResponseComponent = require('hyperspace.components.collision_responses.shockwave')
local ShockwaveSpawnMessage = require('hyperspace.messages.spawners.shockwave')

local CollisionResponseMetaDetecter = require('hyperspace.engines.detecters.meta.collision_response')
local ShockwaveCollisionResponseDetecter = CollisionResponseMetaDetecter(
    'ShockwaveCollisionResponseDetecter',
    ShockwaveCollisionResponseComponent
)

function ShockwaveCollisionResponseDetecter:respond(entity, collision_response_component, contact_component)
    self:create_message(ShockwaveSpawnMessage,
        'x',    contact_component.x1,
        'y',    contact_component.y1,
        'size', collision_response_component.size
    )
end

return ShockwaveCollisionResponseDetecter