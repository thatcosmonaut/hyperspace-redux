local vector = require('lib.hump.vector-light')

local AsteroidSplitSpawnCollisionResponseComponent = require('hyperspace.components.collision_responses.asteroid_split_spawn')
local TransformComponent = require('hyperspace.components.transform')
local VelocityComponent = require('hyperspace.components.velocity')
local AsteroidSizeComponent = require('hyperspace.components.asteroid_size')
local AsteroidSpawnerMessage = require('hyperspace.messages.spawners.asteroid')

local CollisionResponseMetaDetecter = require('hyperspace.engines.detecters.meta.collision_response')

local AsteroidSplitSpawnCollisionResponseDetecter = CollisionResponseMetaDetecter(
    'AsteroidSplitSpawnCollisionResponseDetecter',
    AsteroidSplitSpawnCollisionResponseComponent,
    TransformComponent,
    VelocityComponent,
    AsteroidSizeComponent
)

function AsteroidSplitSpawnCollisionResponseDetecter:respond(_, _, _, transform_components, velocity_components, asteroid_size_components)
    local size = asteroid_size_components[1].size * 0.5
    if size < 25 then return end

    local x_linear, y_linear = velocity_components[1].linear.x, velocity_components[1].linear.y
    local position = transform_components[1].position

    local x_normal, y_normal = vector.normalize(x_linear, y_linear)
    local offset_scalar = size * 0.5 + 10

    local first_x_velocity, first_y_velocity = vector.rotate(-math.pi / 4, x_linear, y_linear)
    local first_x_direction, first_y_direction = vector.mul(offset_scalar, vector.rotate(-math.pi / 4, x_normal, y_normal))
    local first_x_position, first_y_position = vector.add(position.x, position.y, first_x_direction, first_y_direction)

    local second_x_velocity, second_y_velocity = vector.rotate(math.pi / 4, x_linear, y_linear)
    local second_x_direction, second_y_direction = vector.mul(offset_scalar, vector.rotate(math.pi / 4, x_normal, y_normal))
    local second_x_position, second_y_position = vector.add(position.x, position.y, second_x_direction, second_y_direction)

    self:create_message(AsteroidSpawnerMessage,
        'x_position', first_x_position,
        'y_position', first_y_position,
        'x_velocity', first_x_velocity,
        'y_velocity', first_y_velocity,
        'size',       size,
        'color',      {r = 1, g = 1, b = 1, a = 1}
    )

    self:create_message(AsteroidSpawnerMessage,
        'x_position', second_x_position,
        'y_position', second_y_position,
        'x_velocity', second_x_velocity,
        'y_velocity', second_y_velocity,
        'size',       size,
        'color',      {r = 1, g = 1, b = 1, a = 1}
    )
end

return AsteroidSplitSpawnCollisionResponseDetecter