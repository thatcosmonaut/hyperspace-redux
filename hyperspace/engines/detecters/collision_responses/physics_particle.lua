local vector =  require('lib.hump.vector-light')
local MathHelper = require('hyperspace.helpers.math')

local PhysicsParticleCollisionResponseComponent = require('hyperspace.components.collision_responses.physics_particle')
local PhysicsParticleSpawnMessage = require('hyperspace.messages.spawners.physics_particle')

local CollisionResponseMetaDetecter = require('hyperspace.engines.detecters.meta.collision_response')

local PhysicsParticleCollisionResponseDetecter = CollisionResponseMetaDetecter(
    'PhysicsParticleCollisionResponseDetecter',
    PhysicsParticleCollisionResponseComponent
)

function PhysicsParticleCollisionResponseDetecter:respond(_, collision_response_component, contact_component)
    local count = collision_response_component.count
    local x1, y1 = contact_component.x1, contact_component.y1
    local x_normal = contact_component.x_normal
    local y_normal = contact_component.y_normal
    for _ = 1, count do
        local phi = MathHelper.randomFloat(0, 2 * math.pi)
        local x_normal_rotated, y_normal_rotated = vector.rotate(phi, x_normal, y_normal)

        local x_vel, y_vel = vector.mul(40, x_normal_rotated, y_normal_rotated)
        local x_offset, y_offset = vector.mul(5, x_normal_rotated, y_normal_rotated)
        local x_pos, y_pos = vector.add(x1, y1, x_offset, y_offset)

        self:create_message(PhysicsParticleSpawnMessage,
            'x_position', x_pos,
            'y_position', y_pos,
            'x_velocity', x_vel,
            'y_velocity', y_vel
        )
    end
end

return PhysicsParticleCollisionResponseDetecter