local TimeDilationResponseComponent = require('hyperspace.components.collision_responses.time_dilation')
local TimeDilationSpawnMessage = require('hyperspace.messages.spawners.time_dilation')

local CollisionResponseMetaDetecter = require('hyperspace.engines.detecters.meta.collision_response')
local TimeDilationResponseDetecter = CollisionResponseMetaDetecter(
    'TimeDilationResponseDetecter',
    TimeDilationResponseComponent
)

function TimeDilationResponseDetecter:respond(entity, collision_response_component, contact_component)
    self:create_message(TimeDilationSpawnMessage,
        'ease_in_time',  collision_response_component.ease_in_time,
        'time',          collision_response_component.time,
        'ease_out_time', collision_response_component.ease_out_time,
        'factor',        collision_response_component.factor
    )
end

return TimeDilationResponseDetecter