local DestroyCollisionResponseComponent = require('hyperspace.components.collision_responses.destroy')
local DestroyMessage = require('hyperspace.messages.destroy')

local CollisionResponseMetaDetecter = require('hyperspace.engines.detecters.meta.collision_response')
local DestroyCollisionResponseDetecter = CollisionResponseMetaDetecter(
    'DestroyCollisionResponseDetecter',
    DestroyCollisionResponseComponent
)

function DestroyCollisionResponseDetecter:respond(entity, collision_response_component, contact_component)
    self:create_message(DestroyMessage, 'entity', entity)
end

return DestroyCollisionResponseDetecter