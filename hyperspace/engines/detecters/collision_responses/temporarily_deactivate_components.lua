local TemporarilyDeactivateComponentsCollisionResponseComponent = require('hyperspace.components.collision_responses.temporarily_deactivate_components')
local ActivateComponentMessage = require('hyperspace.messages.activate_component')
local DeactivateComponentMessage = require('hyperspace.messages.deactivate_component')
local CreateMessageTimerSpawnMessage = require('hyperspace.messages.spawners.create_message_timer')

local CollisionResponseMetaDetecter = require('hyperspace.engines.detecters.meta.collision_response')
local TemporarilyDeactivateComponentsCollisionResponseDetecter = CollisionResponseMetaDetecter(
    'TemporarilyDeactivateComponentsCollisionResponseDetecter',
    TemporarilyDeactivateComponentsCollisionResponseComponent
)

function TemporarilyDeactivateComponentsCollisionResponseDetecter:respond(entity, collision_response_component, contact_component)
    for _, component_to_deactivate in pairs(collision_response_component.components_to_deactivate) do
        self:create_message(DeactivateComponentMessage,
            'component', component_to_deactivate
        )

        self:create_message(CreateMessageTimerSpawnMessage,
            'time',              collision_response_component.time,
            'message_to_create', ActivateComponentMessage,
            'message_args', {
                'component', component_to_deactivate
            }
        )
    end
end

return TemporarilyDeactivateComponentsCollisionResponseDetecter