local ParticleCollisionResponseComponent = require('hyperspace.components.collision_responses.particle')
local ParticleEmissionMessage = require('hyperspace.messages.particle_emission')

local CollisionResponseMetaDetecter = require('hyperspace.engines.detecters.meta.collision_response')
local ParticleCollisionResponseDetecter = CollisionResponseMetaDetecter(
    'ParticleCollisionResponseDetecter',
    ParticleCollisionResponseComponent
)

function ParticleCollisionResponseDetecter:respond(_, collision_response_component, contact_component)
    self:create_message(ParticleEmissionMessage,
        'particle_system', collision_response_component.particle_system,
        'x',               contact_component.x1,
        'y',               contact_component.y1,
        'amount',          collision_response_component.amount
    )
end

return ParticleCollisionResponseDetecter