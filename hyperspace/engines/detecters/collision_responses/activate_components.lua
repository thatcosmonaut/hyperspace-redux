local ActivateComponentsCollisionResponseComponent = require('hyperspace.components.collision_responses.activate_components')
local ActivateComponentMessage = require('hyperspace.messages.activate_component')

local CollisionResponseMetaDetecter = require('hyperspace.engines.detecters.meta.collision_response')
local ActivateComponentCollisionResponseDetecter = CollisionResponseMetaDetecter(
    'ActivateComponentCollisionResponseDetecter',
    ActivateComponentsCollisionResponseComponent
)

function ActivateComponentCollisionResponseDetecter:respond(entity, collision_response_component, contact_component)
    for _, component_to_activate in pairs(collision_response_component.components_to_activate) do
        self:create_message(ActivateComponentMessage,
            'component', component_to_activate
        )
    end
end

return ActivateComponentCollisionResponseDetecter