local vector = require('lib.hump.vector-light')

local BounceCollisionResponseComponent = require('hyperspace.components.collision_responses.bounce')
local VelocityComponent = require('hyperspace.components.velocity')

local AccelerationMessage = require('hyperspace.messages.acceleration')

local CollisionResponseMetaDetecter = require('hyperspace.engines.detecters.meta.collision_response')
local BounceCollisionResponseDetecter = CollisionResponseMetaDetecter(
    'BounceCollisionResponseDetecter',
    BounceCollisionResponseComponent,
    VelocityComponent
)

function BounceCollisionResponseDetecter:respond(_, _, _, velocity_components)
    local linear = velocity_components[1].linear
    local angular = velocity_components[1].angular

    local new_x_velocity, new_y_velocity = vector.mul(-1, linear.x, linear.y)
    local new_angular = -angular

    local dx, dy = vector.sub(new_x_velocity, new_y_velocity, linear.x, linear.y)
    local angular_delta = new_angular - angular

    self:create_message(AccelerationMessage,
        'component',      velocity_components[1],
        'x_thrust',       dx,
        'y_thrust',       dy,
        'torque',         angular_delta,
        'instant_thrust', true,
        'instant_torque', true
    )
end

return BounceCollisionResponseDetecter