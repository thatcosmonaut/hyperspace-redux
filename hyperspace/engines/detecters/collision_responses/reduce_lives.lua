local LivesComponent = require('hyperspace.components.draw.lives')
local ReduceLivesCollisionResponseComponent = require('hyperspace.components.collision_responses.reduce_lives')

local ReduceLivesMessage = require('hyperspace.messages.reduce_lives')

local CollisionResponseMetaDetecter = require('hyperspace.engines.detecters.meta.collision_response')
local ReduceLivesCollisionResponseDetecter = CollisionResponseMetaDetecter(
    'ReduceLivesCollisionResponseDetecter',
    ReduceLivesCollisionResponseComponent
)

function ReduceLivesCollisionResponseDetecter:respond(entity, collision_response_component, contact_component)
    local lives_component = entity:get_component(LivesComponent)

    self:create_message(ReduceLivesMessage,
        'component', lives_component
    )
end

return ReduceLivesCollisionResponseDetecter