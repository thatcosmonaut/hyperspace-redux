local ContactComponent = require('hyperspace.components.contact')
local RemoveComponentMessage = require('hyperspace.messages.remove_component')

local Detecter = require('lib.encompass').Detecter
local ContactDetecter = Detecter.define('ContactDetecter', { ContactComponent })

function ContactDetecter:detect(entity)
    local contact_components = entity:get_components(ContactComponent)

    for _, contact_component in pairs(contact_components) do
        self:create_message(RemoveComponentMessage,
            'entity', entity,
            'component_to_remove', contact_component
        )
    end
end

return ContactDetecter