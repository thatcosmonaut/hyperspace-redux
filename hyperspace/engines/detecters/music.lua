local MusicComponent = require('hyperspace.components.music')
local MusicMessage = require('hyperspace.messages.music')

local Detecter = require('lib.encompass').Detecter
local MusicDetecter = Detecter.define('MusicDetecter', { MusicComponent })

function MusicDetecter:detect(entity)
    local music_component = entity:get_component(MusicComponent)

    self:create_message(MusicMessage,
        'component', music_component
    )
end

return MusicDetecter