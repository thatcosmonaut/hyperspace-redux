local CreateMessageTimerComponent = require('hyperspace.components.create_message_timer')
local TimerMessage = require('hyperspace.messages.timer')
local RemoveComponentMessage = require('hyperspace.messages.remove_component')

local Detecter = require('lib.encompass').Detecter
local CreateMessageTimerDetecter = Detecter.define('CreateMessageTimerDetecter', { CreateMessageTimerComponent })

function CreateMessageTimerDetecter:detect(entity)
    local timer_components = entity:get_components(CreateMessageTimerComponent)

    for _, timer_component in pairs(timer_components) do
        if timer_component.time <= 0 then
            self:create_message(RemoveComponentMessage,
                'entity',              entity,
                'component_to_remove', timer_component
            )
            self:create_message(timer_component.message_to_create, unpack(timer_component.message_args))
        else
            self:create_message(TimerMessage,
                'component', timer_component
            )
        end
    end
end

return CreateMessageTimerDetecter