PROF_CAPTURE = false
local encompass = require('lib.encompass')

local Hyperspace = require('hyperspace.hyperspace')

function love.load()
    love.window.setMode(1280, 720, { vsync = false, msaa = 2 })
    love.math.setRandomSeed(os.time())
    love.mouse.setVisible(false)

    Hyperspace.load()
end

function love.update(dt)
    Hyperspace.update(dt)
end

function love.draw()
    Hyperspace.draw()
end

function love.quit()
    encompass.prof.write("prof.mpack")
end