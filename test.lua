local class = require "lib.middleclass"

local Test = class('Test')

Test.static.value = 1

local test = Test:new()

print(test)
print(test.class.value)