local Component = {name = "Component"}

function Component.__define_subcomponent_function(super_component_prototype)
    return function(name, required_field_types, optional_field_types)
        local ComponentType = {}
        ComponentType.name = name
        ComponentType.super = super_component_prototype
        ComponentType.field_types = {}
        ComponentType.required_field_types = {}
        ComponentType.optional_field_types = optional_field_types or {}

        setmetatable(ComponentType, {
            __index = super_component_prototype,
            __tostring = function() return name end
        })

        if super_component_prototype.required_field_types ~= nil then
            for k, v in pairs(super_component_prototype.required_field_types) do
                ComponentType.field_types[k] = v
                ComponentType.required_field_types[k] = v
            end
        end

        if required_field_types ~= nil then
            for k, v in pairs(required_field_types) do
                ComponentType.field_types[k] = v
                ComponentType.required_field_types[k] = v
            end
        end

        for k, v in pairs(ComponentType.optional_field_types) do
            assert(
                ComponentType.field_types[k] == nil,
                "Field " .. k .. " on " .. name .. " cannot be both required and optional."
            )
            ComponentType.field_types[k] = v
        end

        return ComponentType
    end
end

Component.define = Component.__define_subcomponent_function(Component)

function Component:__new(entity)
    local component = {}
    setmetatable(component, self)
    self.__index = self
    component.__entity = entity
    component.__active = true
    component.prototype = self
    return component
end

function Component:get_entity()
    return self.__entity
end

function Component:__is_subtype_of(prototype)
    return self == prototype or
        (self.super ~= nil and
        self.super:__is_subtype_of(prototype))
end

function Component:activate()
    if not self.__active then
        table.insert(self.__entity.world.entities_with_added_components, self.__entity)
        self:on_activate()
        self.__active = true
    end
end

function Component:deactivate()
    if self.__active then
        table.insert(self.__entity.world.entities_with_removed_components, self.__entity)
        self:on_deactivate()
        self.__active = false
    end
end

function Component.initialize() end
function Component.on_destroy() end
function Component.on_activate() end
function Component.on_deactivate() end


return Component