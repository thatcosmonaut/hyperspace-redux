local current_folder = (...):gsub('%.[^%.]+$', '') .. '.'

local prof = require(current_folder .. 'lib.jprof')
local utils = require(current_folder .. 'utils')
local Entity = require(current_folder .. 'entity')
local PooledEntity = require(current_folder .. 'pooled_entity')
local Message = require(current_folder .. 'message')
local MessagePool = require(current_folder .. 'message_pool')
local PooledSpawner = require(current_folder .. 'engines.processors.pooled_spawner')
local DrawComponent = require(current_folder .. 'draw_component')

local World = {}

function World:new()
    local world = {}
    setmetatable(world, self)
    self.__index = self

    world.next_id_value = 1

    world.detectors = {}
    world.spawners = {}
    world.modifiers = {}
    world.renderers = {}
    world.side_effecters = {}

    world.world_entities = {}

    world.message_map = {}
    world.message_types = {}
    world.messages = {}
    world.removed_messages = {}
    world.message_to_entity = {}

    world.message_type_to_pool = {}

    world.component_type_to_engine_map = {}
    world.message_type_to_processor_map = {}
    world.state_message_type_to_processor_map = {}

    world.id_to_entity = {}
    world.component_entity_id_map = {}

    world.entities_with_added_components = {}
    world.entities_with_removed_components = {}

    world.marked_for_activation = {}
    world.marked_for_deactivation = {}
    world.marked_for_destroy = {}

    world.draw_layers = {}
    world.draw_order_changed = false
    world.draw_order = {}
    world.draw_component_to_renderer = {}

    return world
end

function World:__next_id()
    local id = self.next_id_value
    self.next_id_value = self.next_id_value + 1
    return id
end

function World:create_entity()
    local id = self:__next_id()
    local __entity = Entity:__new(id, self)

    self.world_entities[__entity] = __entity
    self.id_to_entity[__entity.id] = __entity
    return __entity
end

function World:__create_pooled_entity(spawner)
    local id = self:__next_id()
    local entity = PooledEntity:__new(id, self, spawner)

    self.world_entities[entity] = entity
    self.id_to_entity[entity.id] = entity
    return entity
end

local function process_message_field_args(message_type, message, key, value, ...)
    if key == nil then return end

    -- check that the field is defined in the prototype
    assert(message_type.field_types[key] ~= nil, key .. " is not defined in " .. message_type.name)

    -- check that the given value type is correct as defined in the prototype
    if value ~= nil then -- if value is nil, it may be optional and we will check it later
        assert(
            utils.encompasstype(value, message_type.field_types[key]),
            "value for " ..
                key ..
                    " on " ..
                    message_type.name .. " should be of type " .. tostring(message_type.field_types[key])
        )

        message[key] = value
    end

    process_message_field_args(message_type, message, ...)
end

function World:create_message(message_type, key, value, ...)
    assert(message_type ~= nil, "Message prototype is nil. Did you forget to require?")
    assert(message_type:__is_subtype_of(Message), tostring(message_type) .. " is not a Message prototype")

    local message_pool = self.message_type_to_pool[message_type]
    if message_pool == nil then
        message_pool = MessagePool:__new(self, message_type)
        self.message_type_to_pool[message_type] = message_pool
    end
    local message = message_pool:__get_inactive_message()

    -- check that field arguments have been passed
    if not utils.empty(message_type.required_field_types) then
        if key == nil or value == nil then
            error("Missing field arguments on " .. message_type.name)
        end
    end

    process_message_field_args(message_type, message, key, value, ...)

    -- check that all required fields have been defined
    for k, _ in pairs(message_type.required_field_types) do
        assert(message[k] ~= nil, "missing field " .. k .. " on " .. message_type.name)
    end

    if self.message_map[message_type] == nil then
        self.message_map[message_type] = {}
    end
    table.insert(self.message_map[message_type], message)

    if self.message_types[message_type] == nil then
        self.message_types[message_type] = {}
    end
    table.insert(self.message_types[message_type], message_type)

    self.messages[message] = message

    local referenced_entity = message.entity or utils.lookup_chain(message, "component", "__entity")
    if referenced_entity then
        self.message_to_entity[message] = referenced_entity
    end
    return message
end

function World:__create_component(component_type, __entity)
    local component = component_type:__new(__entity)
    if self.component_entity_id_map[component_type] == nil then
        self.component_entity_id_map[component_type] = {}
    end

    self.component_entity_id_map[component_type][__entity.id] = __entity.id -- break this set logic into a class?
    return component
end

function World:__add_message_processor_relation(processor)
    self.message_type_to_processor_map[processor.prototype.message_type] = processor

    if processor.prototype.state_message_types ~= nil then
        for _, state_component_type in ipairs(processor.prototype.state_message_types) do
            if self.state_message_type_to_processor_map[state_component_type] == nil then
                self.state_message_type_to_processor_map[state_component_type] = {}
            end

            table.insert(self.state_message_type_to_processor_map[state_component_type], processor)
        end
    end
end

function World:__add_component_engine_relation(engine)
    local engine_type = engine.prototype
    for _, component_type in ipairs(engine_type.component_types) do
        if self.component_type_to_engine_map[component_type] == nil then
            self.component_type_to_engine_map[component_type] = {}
        end

        table.insert(self.component_type_to_engine_map[component_type], engine)
    end
end

function World:__register_draw_component_type_renderer_relation(renderer, draw_component_type)
    self.draw_component_to_renderer[draw_component_type] = renderer
end

function World:__deregister_draw_component_type_renderer_relation(draw_component_type)
    self.draw_component_to_renderer[draw_component_type] = nil
end

function World:__remove_draw_component(draw_component)
    self.draw_layers[draw_component.layer][draw_component] = nil
    self.draw_order_changed = true
end

function World:__adjust_component_draw_layers(draw_component, old_layer, new_layer)
    if self.draw_layers[old_layer] ~= nil then
        self.draw_layers[old_layer][draw_component] = nil
    end
    if self.draw_layers[new_layer] == nil then
        self.draw_layers[new_layer] = {}
    end
    self.draw_layers[new_layer][draw_component] = draw_component
    self.draw_order_changed = true
end

function World:add_detector(detector_klass)
    local detecter = detector_klass:__new(self)
    table.insert(self.detectors, detecter)
    self:__add_component_engine_relation(detecter)
    return detecter
end

function World:__add_processor(klass, args)
    local processor
    if args == nil then
        processor = klass:__new(self)
    else
        processor = klass:__new(self, unpack(args))
    end
    return processor
end

function World:add_spawner(spawner_klass, args)
    local spawner = self:__add_processor(spawner_klass, args)
    table.insert(self.spawners, spawner)
    self:__add_message_processor_relation(spawner)
    if spawner_klass:__is_subtype_of(PooledSpawner) then
        spawner:__generate_pooled_entities()
    end
    return spawner
end

function World:add_modifier(modifier_klass, args)
    local modifier = self:__add_processor(modifier_klass, args)
    table.insert(self.modifiers, modifier)
    self:__add_message_processor_relation(modifier)
    return modifier
end

function World:add_renderer(renderer_klass)
    local renderer = renderer_klass:__new(self)
    table.insert(self.renderers, renderer)
    self:__add_component_engine_relation(renderer)
    for _, component_type in pairs(renderer.component_types) do
        if component_type:__is_subtype_of(DrawComponent) then
            self:__register_draw_component_type_renderer_relation(renderer, component_type)
        end
    end
    return renderer
end

function World:add_side_effecter(side_effecter_klass, args)
    local side_effecter = side_effecter_klass:__new(self, unpack(args))
    table.insert(self.side_effecters, side_effecter)
    self:__add_message_processor_relation(side_effecter)
    return side_effecter
end

function World:destroy_all_entities()
    for _, entity in pairs(self.world_entities) do
        if entity.prototype == PooledEntity then
            entity:deactivate()
        else
            entity:destroy()
        end
    end
    self:__destroy_marked_entities()
end

function World:__clear_message(message)
    self.removed_messages[message] = message
    self.messages[message] = nil
    utils.remove(self.message_types[message.prototype], message.prototype)
    utils.remove(self.message_map[message.prototype], message)
end

function World:__destroy_messages()
    for k, message in pairs(self.messages) do
        message:__deactivate()
        self.messages[k] = nil
    end

    utils.clear(self.removed_messages)
    utils.clear(self.message_to_entity)
end

function World:__clear_messages_for_entity(given_entity)
    for message, entity in pairs(self.message_to_entity) do
        if entity == given_entity then
            self:__clear_message(message)
        end
    end
end

function World:__check_entities_with_updated_components()
    for k, entity in pairs(self.entities_with_added_components) do
        self:__check_and_register_entity(entity)
        self.entities_with_added_components[k] = nil
    end

    for k, entity in pairs(self.entities_with_removed_components) do
        for _, engine in pairs(entity.tracked_by) do
            engine:__check_and_untrack_entity(entity)
        end
        self.entities_with_removed_components[k] = nil
    end
end

function World:__check_entity_activation_status()
    for k, __entity in pairs(self.marked_for_deactivation) do
        __entity:__deactivate()
        self.marked_for_deactivation[k] = nil
    end

    for k, __entity in pairs(self.marked_for_activation) do
        __entity:__activate()
        self.marked_for_activation[k] = nil
    end
end

function World:update(dt)
    prof.push("frame")
    prof.push("World:update")
    prof.push("World:__check_entities_with_updated_components")
    self:__check_entities_with_updated_components()
    prof.pop("World:__check_entities_with_updated_components")
    prof.push("World:__check_entity_activation_status")
    self:__check_entity_activation_status()
    prof.pop("World:__check_entity_activation_status")
    prof.push("World:__detect")
    self:__detect()
    prof.pop("World:__detect")
    prof.push("World:__check_messages")
    self:__check_messages()
    prof.pop("World:__check_messages")
    prof.push("World:__spawn")
    self:__spawn()
    prof.pop("World:__spawn")
    prof.push("World:__untrack_respawned_entities")
    self:__untrack_respawned_entities()
    prof.pop("World:__untrack_respawned_entities")
    prof.push("World:__modify")
    self:__modify(dt)
    prof.pop("World:__modify")
    prof.push("World:__destroy_marked_entities")
    self:__destroy_marked_entities()
    prof.pop("World:__destroy_marked_entities")
    if self.draw_order_changed then
        prof.push("World:__recalculate_draw_order")
        self:__recalculate_draw_order()
        prof.pop("World:__recalculate_draw_order")
        self.draw_order_changed = false
    end
    prof.push("World:__side_effects")
    self:__side_effects(dt)
    prof.pop("World:__side_effects")
    prof.push("World:__destroy_messages")
    self:__destroy_messages()
    prof.pop("World:__destroy_messages")
    prof.pop("World:update")
end

function World:__detect()
    for _, detecter in ipairs(self.detectors) do
        prof.push(detecter.prototype.name .. ":detect")
        detecter:__update()
        prof.pop(detecter.prototype.name .. ":detect")
    end
end

function World:__check_messages()
    for _, message in pairs(self.messages) do
        if self.message_type_to_processor_map[message.prototype] ~= nil then
            local processor = self.message_type_to_processor_map[message.prototype]
            processor:check_and_track_message(message)
        elseif self.state_message_type_to_processor_map[message.prototype] ~= nil then
            for _, processor in pairs(self.state_message_type_to_processor_map[message.prototype]) do
                processor:__check_and_track_state_message(message)
            end
        end
    end
end

function World:__spawn()
    for _, spawner in ipairs(self.spawners) do
        prof.push(spawner.prototype.name .. ":spawn")
        spawner:__update()
        prof.pop(spawner.prototype.name .. ":spawn")
        spawner:__clean()
    end
end

function World:__untrack_respawned_entities()
    for _, message in pairs(self.removed_messages) do
        if self.message_type_to_processor_map[message.prototype] ~= nil then
            local processor = self.message_type_to_processor_map[message.prototype]
            processor:__untrack_message(message)
        end
    end
end

function World:__modify(dt)
    for _, modifier in ipairs(self.modifiers) do
        prof.push(modifier.prototype.name .. ":modify")
        modifier:__update(dt)
        prof.pop(modifier.prototype.name .. ":modify")
        modifier:__clean()
    end
end

function World:__recalculate_draw_order()
    utils.clear(self.draw_order)
    for _, components in utils.ordered_pairs(self.draw_layers) do
        for _, component in pairs(components) do
            table.insert(self.draw_order, component)
        end
    end
end

function World:draw(canvas)
    prof.push("World:draw")
    for _, component in pairs(self.draw_order) do
        local renderer = self.draw_component_to_renderer[component.prototype]
        local entity = component.__entity
        if renderer.tracked_entities[component.__entity] ~= nil then
            prof.push(renderer.prototype.name .. ":render")
            renderer:render(entity, canvas)
            prof.pop(renderer.prototype.name .. ":render")
        end
    end
    prof.pop("World:draw")
    prof.pop("frame")
end

function World:__side_effects(dt)
    for _, side_effecter in pairs(self.side_effecters) do
        prof.push(side_effecter.prototype.name .. ":effect")
        side_effecter:__update(dt)
        prof.pop(side_effecter.prototype.name .. ":effect")
        side_effecter:__clean()
    end
end

function World:__check_and_register_entity(entity)
    for _, component in pairs(entity.components) do
        if self.component_type_to_engine_map[component.prototype] ~= nil then
            for _, engine in pairs(self.component_type_to_engine_map[component.prototype]) do
                engine:__check_and_track_entity(entity)
            end
        end
    end
end

function World:find_entity(id)
    return self.id_to_entity[id]
end

function World:__mark_entity_for_activation(__entity)
    self.marked_for_activation[__entity] = __entity
end

function World:__mark_entity_for_deactivation(__entity)
    self.marked_for_deactivation[__entity] = __entity
end

function World:__mark_entity_for_destroy(__entity)
    self.marked_for_destroy[__entity] = __entity
end

function World:__destroy_marked_entities()
    for k, entity in pairs(self.marked_for_destroy) do
        self:__destroy_entity(entity)
        self.marked_for_destroy[k] = nil
    end
end

function World:__destroy_entity(__entity)
    for _, engine in pairs(__entity.tracked_by) do
        engine:__untrack_entity(__entity)
    end
    for _, component in pairs(__entity.components) do
        __entity:remove_component(component)
    end
    self.world_entities[__entity] = nil
    self.id_to_entity[__entity.id] = nil
    self.entities_with_added_components[__entity] = nil
    self.entities_with_removed_components[__entity] = nil
end

return World