local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local Message = require(up_one_folder .. 'message')

local SideEffectMessage = Message.define("SideEffectMessage")
SideEffectMessage.define = Message.__define_submessage_function(SideEffectMessage)

return SideEffectMessage