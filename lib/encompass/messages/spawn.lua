local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local Message = require(up_one_folder .. 'message')

local SpawnMessage = Message.define("SpawnMessage")
SpawnMessage.define = Message.__define_submessage_function(SpawnMessage)

return SpawnMessage