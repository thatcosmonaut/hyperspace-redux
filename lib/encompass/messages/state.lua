local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local Message = require(up_one_folder .. 'message')

local StateMessage = Message.define("StateMessage")
StateMessage.define = Message.__define_submessage_function(StateMessage)

return StateMessage