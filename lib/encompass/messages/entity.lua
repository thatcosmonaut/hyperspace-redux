local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local Entity = require(up_one_folder .. 'entity')
local Message = require(up_one_folder .. 'message')

local EntityMessage = Message.define("EntityMessage", { entity = Entity })
EntityMessage.define = Message.__define_submessage_function(EntityMessage)

return EntityMessage