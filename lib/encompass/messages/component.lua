local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local Component = require(up_one_folder .. 'component')
local Message = require(up_one_folder .. 'message')

local ComponentMessage = Message.define("ComponentMessage", { component = Component })
ComponentMessage.define = Message.__define_submessage_function(ComponentMessage)

return ComponentMessage