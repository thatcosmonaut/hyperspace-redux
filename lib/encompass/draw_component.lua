local _prefix = (...):match("(.+%.)[^%.]+$") or ""

local Component = require(_prefix .. 'component')

local DrawComponent = Component.define("DrawComponent", {layer = "number"})
DrawComponent.define = function(name, required_field_types, optional_field_types)
    local SubDrawComponentType = Component.__define_subcomponent_function(DrawComponent)(
        name,
        required_field_types,
        optional_field_types
    )

    local draw_component_metatable = {}

    -- getter and setter function for layer to recalculate world draw layers
    draw_component_metatable.__index = function(self, key)
        if key == 'layer' then
            return rawget(self, '__layer')
        else
            return rawget(self, key) or SubDrawComponentType[key]
        end
    end

    draw_component_metatable.__newindex = function(self, key, value)
        if key == 'layer' then
            if rawget(self, '__layer') == value then return end
            self.__entity.world:__adjust_component_draw_layers(self, rawget(self, '__layer'), value)
            rawset(self, '__layer', value)
        else
            rawset(self, key, value)
        end
    end

    function SubDrawComponentType:__new(entity)
        local component = Component.__new(self, entity)
        component.prototype = self
        setmetatable(component, draw_component_metatable)
        return component
    end

    return SubDrawComponentType
end

return DrawComponent