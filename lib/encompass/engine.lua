local Engine = {}

function Engine.__define_engine_function(super_engine, do_assert)
    return function(name, component_types)
        if do_assert then
            assert(
                component_types ~= nil and next(component_types) ~= nil,
                super_engine.name .. " " .. tostring(name) .. " has no component types"
            )
        end

        local EngineType = {}
        EngineType.name = name
        EngineType.component_types = component_types

        setmetatable(
            EngineType,
            {
                __index = super_engine,
                __tostring = function()
                    return name
                end
            }
        )

        EngineType.is_of_type = function(engine_type)
            return engine_type == super_engine
        end

        return EngineType
    end
end

Engine.define = Engine.__define_engine_function(Engine, false)

function Engine:__new(world, ...)
    local engine = {}
    self.__index = self
    setmetatable(engine, self)

    engine.prototype = self
    engine.world = world
    engine.tracked_entities = {}

    engine.deactivated_entities = {}

    engine:initialize(...)
    return engine
end

function Engine.initialize() end

function Engine:__is_subtype_of(prototype)
    return self == prototype or
        (self.super ~= nil and self.super:__is_subtype_of(prototype))
end

function Engine:__check_entity(__entity)
    -- want to be false if there are no component types on the detecter for some reason
    if next(self.prototype.component_types) == nil then
        return false
    end

    local has_all_components = true

    for _, component_type in ipairs(self.prototype.component_types) do
        has_all_components = has_all_components and __entity:__has_active_component(component_type)
    end

    return has_all_components
end

function Engine:__track_entity(__entity)
    if self.tracked_entities[__entity] ~= nil then
        return
    end

    self.tracked_entities[__entity] = __entity
    __entity.tracked_by[self] = self
end

function Engine:__untrack_entity(__entity)
    self.tracked_entities[__entity] = nil
    self.deactivated_entities[__entity] = nil
    __entity.tracked_by[self] = nil
end

function Engine:__activate_entity(__entity)
    if self.deactivated_entities[__entity] ~= nil then
        self.tracked_entities[__entity] = __entity
        self.deactivated_entities[__entity] = nil
    end
end

function Engine:__deactivate_entity(__entity)
    if self.tracked_entities[__entity] ~= nil then
        self.deactivated_entities[__entity] = __entity
        self.tracked_entities[__entity] = nil
    end
end

return Engine