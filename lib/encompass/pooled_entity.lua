local current_folder = (...):gsub('%.[^%.]+$', '') .. '.'

local Entity = require(current_folder .. 'entity')

local PooledEntity = {name = "PooledEntity", super = Entity}
setmetatable(PooledEntity, {
    __index = Entity,
    __tostring = function()
        return PooledEntity.name
    end
})

function PooledEntity:__new(id, world, spawner)
    local entity = Entity.__new(self, id, world)
    entity.spawner = spawner
    return entity
end

function PooledEntity:deactivate()
    self.world:__mark_entity_for_deactivation(self)
    self.spawner:__deactivate_entity(self)
end

function PooledEntity.destroy()
    error("cannot destroy a pooled entity")
end

return PooledEntity