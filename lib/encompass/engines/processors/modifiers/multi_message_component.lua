local current_folder = (...):gsub('%.[^%.]+$', '') .. '.'
local up_three_folders = (...):gsub('%.[^%.]+$', '')
                              :gsub('%.[^%.]+$', '')
                              :gsub('%.[^%.]+$', '')
                              :gsub('%.[^%.]+$', '')
                              .. '.'

local utils = require(up_three_folders .. 'utils')
local ComponentModifier = require(current_folder .. 'component')

local MultiMessageComponentModifier = {name = "MultiMessageComponentModifier", super = ComponentModifier}
setmetatable(MultiMessageComponentModifier, {__index = ComponentModifier})

MultiMessageComponentModifier.define = function(name, message_type, state_message_types)
    local MultiMessageComponentModifierType = ComponentModifier.define(name, message_type, state_message_types)
    MultiMessageComponentModifierType.super = MultiMessageComponentModifier
    setmetatable(MultiMessageComponentModifierType, {
        __index = MultiMessageComponentModifier
    })

    return MultiMessageComponentModifierType
end

function MultiMessageComponentModifier:__new(world, ...)
    local component_modifier = ComponentModifier.__new(self, world, ...)

    self.component_to_messages = {}
    return component_modifier
end

function MultiMessageComponentModifier:__update(dt)
    for component, messages in pairs(self.component_to_messages) do
        if #messages > 0 then
            self:modify(component, self:__create_frozen_fields(component), messages, dt)
        end
    end
end

function MultiMessageComponentModifier:modify()
    error("modify function should be implemented on " .. tostring(self.prototype))
end

function MultiMessageComponentModifier:__track_message(message)
    if self.component_to_messages[message.component] == nil then
        self.component_to_messages[message.component] = {}
    end
    table.insert(self.component_to_messages[message.component], message)
end

function MultiMessageComponentModifier:__untrack_message(message)
    utils.remove(self.component_to_messages[message.component], message)
end

function MultiMessageComponentModifier:__clean()
    utils.clear(self.state_messages)
    -- dont throw away internal tables
    for _, t in pairs(self.component_to_messages) do
        utils.clear(t)
    end
end

return MultiMessageComponentModifier