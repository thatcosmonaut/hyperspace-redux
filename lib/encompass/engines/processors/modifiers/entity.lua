local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'
local up_three_folders = (...):gsub('%.[^%.]+$', '')
                              :gsub('%.[^%.]+$', '')
                              :gsub('%.[^%.]+$', '')
                              :gsub('%.[^%.]+$', '')
                              .. '.'

local utils = require(up_three_folders .. 'utils')
local EntityMessage = require(up_three_folders .. 'messages.entity')
local Modifier = require(up_one_folder .. 'modifier')

local EntityModifier = {name = "EntityModifier", super = Modifier}
setmetatable(EntityModifier, {__index = Modifier})

EntityModifier.define = function(name, message_type, state_message_types)
    local EntityModifierType = Modifier.define(name, message_type, state_message_types)

    assert(
        message_type:__is_subtype_of(EntityMessage),
        "EntityModifier "
            .. name
                .. " must consume a message of EntityMessage type"
    )

    EntityModifierType.super = EntityModifier
    setmetatable(EntityModifierType, {
        __index = EntityModifier
    })

    return EntityModifierType
end

function EntityModifier:__new(world, ...)
    local entity_modifier = Modifier.__new(self, world, ...)

    self.entity_to_messages = {}
    return entity_modifier
end

function EntityModifier:__update(dt)
    for entity, messages in pairs(self.entity_to_messages) do
        if #messages > 0 then
            self:modify(entity, messages, dt)
        end
    end
end

function EntityModifier:modify()
    error("modify function should be implemented on " .. tostring(self.prototype))
end

function EntityModifier:__track_message(message)
    if self.entity_to_messages[message.entity] == nil then
        self.entity_to_messages[message.entity] = {}
    end
    table.insert(self.entity_to_messages[message.entity], message)
end

function EntityModifier:__untrack_message(message)
    utils.remove(self.entity_to_messages[message.entity], message)
end

function EntityModifier:__clean()
    utils.clear(self.state_messages)
    -- dont throw away internal tables
    for _, t in pairs(self.entity_to_messages) do
        utils.clear(t)
    end
end

return EntityModifier