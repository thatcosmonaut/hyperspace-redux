local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'
local up_three_folders = (...):gsub('%.[^%.]+$', '')
                              :gsub('%.[^%.]+$', '')
                              :gsub('%.[^%.]+$', '')
                              :gsub('%.[^%.]+$', '')
                              .. '.'

local utils = require(up_three_folders .. 'utils')
local Modifier = require(up_one_folder .. 'modifier')
local ComponentMessage = require(up_three_folders .. 'messages.component')

local ComponentModifier = {name = "ComponentModifier", super = Modifier}
setmetatable(ComponentModifier, {__index = Modifier})
ComponentModifier.define = function(name, message_type, state_message_types)
    local ComponentModifierType = Modifier.define(name, message_type, state_message_types)

    assert(message_type:__is_subtype_of(ComponentMessage), name .. " must consume a message of ComponentMessage type")
    ComponentModifierType.super = ComponentModifier
    setmetatable(ComponentModifierType, {
        __index = ComponentModifier
    })

    return ComponentModifierType
end

function ComponentModifier:__new(world, ...)
    local component_modifier = Modifier.__new(self, world, ...)

    self.component_to_message = {}
    return component_modifier
end

function ComponentModifier:__update(dt)
    for component, message in pairs(self.component_to_message) do
        self:modify(component, self:__create_frozen_fields(component), message, dt)
    end
end

function ComponentModifier:modify()
    error("modify function should be implemented on " .. tostring(self.prototype))
end

function ComponentModifier:__track_message(message)
    assert(
        self.component_to_message[message.component] == nil,
        tostring(self.prototype) .. " caught multiple messages for the same component"
    )
    self.component_to_message[message.component] = message
end

function ComponentModifier:__untrack_message(message)
    self.component_to_message[message.component] = nil
end

function ComponentModifier:__clean()
    utils.clear(self.state_messages)
    utils.clear(self.component_to_message)
end

return ComponentModifier