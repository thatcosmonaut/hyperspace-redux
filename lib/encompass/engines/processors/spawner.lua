local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'
local up_two_folders = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local utils = require(up_two_folders .. 'utils')

local Processor = require(up_one_folder .. 'processor')
local SpawnMessage = require(up_two_folders .. 'messages.spawn')

local Spawner = {name = "Spawner", super = Processor}
setmetatable(Spawner, {__index = Processor})

Spawner.define = function(name, message_type)
    local SpawnerType = Processor.define(name, message_type)
    assert(message_type:__is_subtype_of(SpawnMessage), name .. " must consume a message of SpawnMessage type")
    SpawnerType.super = Spawner
    setmetatable(SpawnerType, {
        __index = Spawner
    })
    return SpawnerType
end

function Spawner:__new(world, ...)
    local spawner = Processor.__new(self, world)

    spawner.messages = {}
    spawner:initialize(...)
    return spawner
end

function Spawner.initialize() end

function Spawner:__update()
    for _, message in pairs(self.messages) do
        self:spawn(message)
    end
end

function Spawner:spawn()
    error("spawn function should be implemented on " .. tostring(self.prototype))
end

function Spawner:__track_message(message)
    table.insert(self.messages, message)
end

function Spawner:__clean()
    utils.clear(self.state_messages)
    utils.clear(self.messages)
end

return Spawner