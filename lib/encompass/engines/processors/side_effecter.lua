local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'
local up_two_folders = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local utils = require(up_two_folders .. 'utils')
local SideEffectMessage = require(up_two_folders .. 'messages.side_effect')
local Processor = require(up_one_folder .. 'processor')

local SideEffecter = {name = "SideEffecter", super = Processor}
setmetatable(SideEffecter, {__index = Processor})
SideEffecter.define = function(name, message_type, state_message_types)
    local SideEffecterType = Processor.define(name, message_type, state_message_types)

    assert(
        message_type:__is_subtype_of(SideEffectMessage),
        "SideEffecter "
            .. name
                .. " must consume a message of SideEffectMessage type"
    )

    SideEffecterType.super = SideEffecter
    setmetatable(SideEffecterType, {__index = SideEffecter})
    return SideEffecterType
end

function SideEffecter:__new(world, ...)
    local side_effecter = Processor.__new(self, world)

    side_effecter.messages = {}
    side_effecter:initialize(...)
    return side_effecter
end

function SideEffecter:__track_message(message)
    table.insert(self.messages, message)
end

function SideEffecter:__update(dt)
    for _, message in pairs(self.messages) do
        self:effect(message, dt)
    end
end

function SideEffecter:effect()
    error("effect function should be implemented on " .. tostring(self.prototype))
end

function SideEffecter:__clean()
    utils.clear(self.messages)
end

return SideEffecter