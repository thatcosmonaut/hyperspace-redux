local current_folder = (...):gsub('%.[^%.]+$', '') .. '.'

local Spawner = require(current_folder .. 'spawner')

local PooledSpawner = {name = "PooledSpawner", super = Spawner}
setmetatable(PooledSpawner, {__index = Spawner})

PooledSpawner.OverflowBehaviors = {
    expand = function(spawner)
        spawner.pool_count = spawner.pool_count + 1
        return spawner:__create_entity()
    end,
    throw = function(spawner)
        return spawner:throw()
    end,
    fallible = function() end,
    cycle = function(spawner)
        local entity = next(spawner.active_entities, spawner.previous_overflow_entity)
        spawner.world:__clear_messages_for_entity(entity)
        return entity
    end
}

PooledSpawner.define = function(name, message_type, pool_count, pool_overflow_behavior)
    local PooledSpawnerType = Spawner.define(name, message_type)

    assert(type(pool_count) == 'number', name .. " should be defined with a numerical pool count")
    PooledSpawnerType.super = PooledSpawner
    setmetatable(PooledSpawnerType, {
        __index = PooledSpawner,
        __tostring = function() return name end
    })

    PooledSpawnerType.pool_count = pool_count
    PooledSpawnerType.overflow_behavior = pool_overflow_behavior or PooledSpawner.OverflowBehaviors.fallible

    return PooledSpawnerType
end

function PooledSpawner:__new(world, ...)
    local pooled_spawner = Spawner.__new(self, world)

    pooled_spawner.active_entities = {}
    pooled_spawner.inactive_entities = {}

    pooled_spawner:initialize(...)
    return pooled_spawner
end

function PooledSpawner.initialize() end

function PooledSpawner:__create_entity()
    local entity = self.world:__create_pooled_entity(self)
    entity.spawner = self
    self:generate(entity)
    entity:deactivate()
    return entity
end

function PooledSpawner:__generate_pooled_entities()
    for _ = 1, self.pool_count do
        self:__create_entity()
    end
end

function PooledSpawner:generate()
    error("generate function should be implemented on " .. tostring(self.prototype))
end

function PooledSpawner:__get_inactive_entity()
    local entity = next(self.inactive_entities)
    if entity == nil then
        entity = self:overflow_behavior()
        self.previous_overflow_entity = entity
    end
    return entity
end

function PooledSpawner:__activate_inactive_entity()
    local entity = self:__get_inactive_entity()
    if entity ~= nil then
        entity:activate()
        self.inactive_entities[entity] = nil
        self.active_entities[entity] = entity
        return entity
    end
end

function PooledSpawner:__deactivate_entity(entity)
    self.active_entities[entity] = nil
    self.inactive_entities[entity] = entity
end

function PooledSpawner:__update()
    for _, message in pairs(self.messages) do
        local entity = self:__activate_inactive_entity()
        if entity ~= nil then
            self:spawn(entity, message)
        end
    end
end

function PooledSpawner:spawn()
    error("spawn function should be implemented on " .. tostring(self.prototype))
end

return PooledSpawner