local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local Processor = require(up_one_folder .. 'processor')

local Modifier = {name = "Modifier", super = Processor}
setmetatable(Modifier, {__index = Processor})

Modifier.define = function(name, message_type, state_message_types)
    local ModifierType = Processor.define(name, message_type, state_message_types)
    ModifierType.super = Modifier
    setmetatable(ModifierType, {
        __index = Modifier
    })
    return ModifierType
end

function Modifier:__new(world, ...)
    local modifier = Processor.__new(self, world)
    self.component_type_to_frozen_field_table = {}

    modifier:initialize(...)
    return modifier
end

function Modifier.initialize() end

function Modifier:__create_frozen_fields(component)
    if self.component_type_to_frozen_field_table[component.prototype] == nil then
        self.component_type_to_frozen_field_table[component.prototype] = {}
    end
    local frozen_fields = self.component_type_to_frozen_field_table[component.prototype]
    for key, _ in pairs(component.prototype.field_types) do
        frozen_fields[key] = component[key]
    end
    return frozen_fields
end

return Modifier