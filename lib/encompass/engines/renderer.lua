local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local DrawComponent = require(up_one_folder .. 'draw_component')
local Engine = require(up_one_folder .. 'engine')

local Renderer = Engine.define("Renderer")
Renderer.define = function(name, component_types)
    assert(
        component_types ~= nil and next(component_types) ~= nil,
        Renderer.name .. " " .. tostring(name) .. " has no component types"
    )
    local draw_component_count = 0
    for _, component_type in pairs(component_types) do
        if component_type:__is_subtype_of(DrawComponent) then
            draw_component_count = draw_component_count + 1
        end
    end
    if draw_component_count ~= 1 then error("Renderer " .. name .. " must track exactly one DrawComponent") end
    return Engine.__define_engine_function(Renderer, false)(name, component_types)
end

function Renderer:render()
    error("render should be overridden on " .. tostring(self.class))
end

function Renderer:__track_entity(__entity)
    Engine.__track_entity(self, __entity)
end

function Renderer:__untrack_entity(__entity)
    Engine.__untrack_entity(self, __entity)
end

function Renderer:__check_and_track_entity(__entity)
    if self:__check_entity(__entity) then
        self:__track_entity(__entity)
    end
end

function Renderer:__check_and_untrack_entity(__entity)
    if not self:__check_entity(__entity) then
        self:__untrack_entity(__entity)
    end
end

function Renderer:__activate_entity(__entity)
    Engine.__activate_entity(self, __entity)
end

function Renderer:__deactivate_entity(__entity)
    Engine.__deactivate_entity(self, __entity)
end

return Renderer