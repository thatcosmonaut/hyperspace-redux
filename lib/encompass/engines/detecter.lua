local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local Engine = require(up_one_folder .. 'engine')

local Detecter = Engine.define("Detecter")
Detecter.define = Engine.__define_engine_function(Detecter, true)

function Detecter:__update()
    for _, entity in pairs(self.tracked_entities) do
        self:detect(entity)
    end
end

function Detecter:detect()
    error("detect should be overridden on " .. tostring(self.prototype))
end

function Detecter:create_message(message, ...)
    return self.world:create_message(message, ...)
end

function Detecter:__check_and_track_entity(__entity)
    if self:__check_entity(__entity) then
        self:__track_entity(__entity)
    end
end

function Detecter:__check_and_untrack_entity(__entity)
    if not self:__check_entity(__entity) then
        self:__untrack_entity(__entity)
    end
end

return Detecter