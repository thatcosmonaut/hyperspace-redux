local up_one_folder = (...):gsub('%.[^%.]+$', ''):gsub('%.[^%.]+$', '') .. '.'

local utils = require(up_one_folder .. 'utils')
local Engine = require(up_one_folder .. 'engine')

local Processor = {name = "Processor", super = Engine}
setmetatable(Processor, {
    __index = Engine,
    __tostring = function() return Processor.name end
})

Processor.define = function(name, message_type, state_message_types)
    assert(
        message_type ~= nil and not utils.empty(message_type),
        name .. " requires a message prototype"
    )

    local ProcessorPrototype = {}
    ProcessorPrototype.name = name
    ProcessorPrototype.message_type = message_type
    ProcessorPrototype.state_message_types = state_message_types
    ProcessorPrototype.super = Processor

    setmetatable(ProcessorPrototype, {
        __index = Processor,
        __tostring = function() return name end
    })

    return ProcessorPrototype
end

function Processor:__new(world)
    local processor = Engine.__new(self, world)

    processor.prototype = self
    processor.world = world
    processor.state_messages = {}

    return processor
end

function Processor:get_state_message(state_message_type)
    return self.state_messages[state_message_type]
end

function Processor:create_entity()
    return self.world:create_entity()
end

function Processor:__check_message(message)
    return message.prototype == self.message_type
end

function Processor:check_and_track_message(message)
    if self:__check_message(message) then
        self:__track_message(message)
    end
end

function Processor:__check_and_track_state_message(state_message)
    for _, state_message_type in ipairs(self.state_message_types) do
        if state_message.prototype == state_message_type then
            self.state_messages[state_message.prototype] = state_message
        end
    end
end

return Processor