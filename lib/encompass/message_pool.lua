local MessagePool = { name = "MessagePool" }

function MessagePool:__new(world, message_type)
    local message_pool = {}
    setmetatable(message_pool, self)
    self.__index = self
    self.prototype = self

    message_pool.world = world
    message_pool.message_type = message_type
    message_pool.size = 16

    message_pool.inactive_messages = {}
    message_pool.active_messages = {}

    for _ = 1, message_pool.size do
        local empty_message = message_type:__new(message_pool)
        message_pool.inactive_messages[empty_message] = empty_message
    end

    return message_pool
end

function MessagePool:__get_inactive_message()
    local message = next(self.inactive_messages)
    if message == nil then
        for _ = self.size, self.size * 2 do
            local empty_message = self.message_type:__new(self)
            self.inactive_messages[empty_message] = empty_message
        end
        self.size = self.size * 2
        message = next(self.inactive_messages)
    end
    self.inactive_messages[message] = nil
    self.active_messages[message] = message
    return message
end

function MessagePool:__deactivate_message(message)
    self.active_messages[message] = nil
    self.inactive_messages[message] = message
end

return MessagePool