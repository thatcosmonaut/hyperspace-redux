local current_folder = (...):gsub('%.[^%.]+$', '') .. '.'

local Component = require(current_folder .. 'component')

local Message = {name = "Message"}
Message.__define_submessage_function = Component.__define_subcomponent_function
Message.define = Message.__define_submessage_function(Message)

function Message:__new(message_pool)
    local message = {}
    setmetatable(message, self)
    self.__index = self
    message.prototype = self
    message.pool = message_pool
    return message
end

function Message:__is_subtype_of(prototype)
    return self == prototype or
        (self.super ~= nil and
        self.super:__is_subtype_of(prototype))
end

function Message:__deactivate()
    self.pool:__deactivate_message(self)
end

return Message