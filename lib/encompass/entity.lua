local _prefix = (...):match("(.+%.)[^%.]+$") or ""

local utils = require(_prefix .. 'utils')
local Component = require(_prefix .. 'component')
local DrawComponent = require(_prefix .. 'draw_component')

local Entity = {name = "Entity"}
setmetatable(Entity, {
    __tostring = function()
        return Entity.name
    end
})

function Entity:__new(id, world)
    local entity = {}
    setmetatable(entity, self)
    self.__index = self
    self.prototype = self

    entity.id = id
    entity.world = world

    entity.active = true

    entity.marked_for_activation = false
    entity.marked_for_deactivation = false

    entity.component_map = {}
    entity.component_types = {}

    entity.components = {}
    entity.draw_components = {}
    entity.tracked_by = {}

    return entity
end

local function process_component_field_args(component_type, component, key, value, ...)
    if key == nil then return end

    -- check that the field is defined in the prototype
    assert(component_type.field_types[key] ~= nil, key .. " is not defined in " .. component_type.name)

    -- check that the given value type is correct as defined in the prototype
    if value ~= nil then -- if value is nil, it may be optional and we'll check it later
        assert(
            utils.encompasstype(value, component_type.field_types[key]),
            "value for " ..
                key ..
                    " on " ..
                        component_type.name .. " should be of type " .. tostring(component_type.field_types[key])
        )

        component[key] = value
    end

    process_component_field_args(component_type, component, ...)
end

function Entity:__register_component(component_type, key, value, ...)
    assert(component_type ~= nil, "SuperComponent is nil. Did you forget to require?")

    local component = self.world:__create_component(component_type, self)

    if not utils.empty(component_type.required_field_types) then
        if key == nil or value == nil then
            error("Missing field arguments on " .. component_type.name)
        end
    end

    process_component_field_args(component_type, component, key, value, ...)

    for k, _ in pairs(component_type.required_field_types) do
        assert(component[k] ~= nil, "missing field " .. k .. " on " .. component_type.name)
    end

    if self.component_map[component_type] == nil then
        self.component_map[component_type] = {}
    end
    table.insert(self.component_map[component_type], component)
    if self.component_types[component_type] == nil then
        self.component_types[component_type] = {}
    end
    table.insert(self.component_types[component_type], component_type)
    self.components[component] = component

    component:initialize()

    return component
end

function Entity:add_component(component_type, ...)
    if component_type == nil then
        error("Component is nil. Did you forget to require?")
    end
    if not component_type:__is_subtype_of(Component) then
        error("Attempted to add a non Component to an Entity")
    end
    local component = self:__register_component(component_type, ...)
    table.insert(self.world.entities_with_added_components, self)
    if component_type:__is_subtype_of(DrawComponent) then
        self.draw_components[component] = component
    end
    return component
end

function Entity:remove_component(component)
    assert(
        self.components[component] ~= nil,
        "Entity " ..
            self.id ..
                " does not have component "
                    .. tostring(component)
    )

    local component_type = component.prototype

    self.components[component] = nil

    if component:__is_subtype_of(DrawComponent) then
        self.draw_components[component] = nil
        self.world:__remove_draw_component(component)
    end

    utils.remove(self.component_map[component_type], component)
    utils.remove(self.component_types[component_type], component_type)

    component:on_destroy()

    table.insert(self.world.entities_with_removed_components, self)
end

function Entity:get_component(component_type)
    if not self:has_component(component_type) then
        error("Entity does not have component of type " .. component_type.name)
    end
    return self.component_map[component_type][1]
end

function Entity:get_components(component_type)
    if not self:has_component(component_type) then
        error("Entity does not have component of type " .. component_type.name)
    end
    return self.component_map[component_type]
end

function Entity:has_component(component_type)
    return self.component_types[component_type] ~= nil and #self.component_types[component_type] ~= 0
end

function Entity:__has_active_component(component_type)
    if not self:has_component(component_type) then return false end
    local components = self:get_components(component_type)
    for _, component in pairs(components) do
        if component.__active then return true end
    end
    return false
end

function Entity:destroy()
    self.world:__mark_entity_for_destroy(self)
end

function Entity:activate()
    self.world:__mark_entity_for_activation(self)
end

function Entity:__activate()
    if not self.active then
        self.active = true
        for _, component in pairs(self.components) do
            if component.__active then
                component:on_activate()
            end
        end
        for _, engine in pairs(self.tracked_by) do
            engine:__activate_entity(self)
        end
    end
end

function Entity:deactivate()
    self.world:__mark_entity_for_deactivation(self)
end

function Entity:__deactivate()
    if self.active then
        self.active = false
        for _, component in pairs(self.components) do
            if component.__active then
                component:on_deactivate()
            end
        end
        for _, engine in pairs(self.tracked_by) do
            engine:__deactivate_entity(self)
        end
    end
end

return Entity