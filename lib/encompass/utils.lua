local utils = {}

function utils.empty(t)
    return next(t) == nil
end

function utils.clear(t)
    for k in pairs(t) do
        t[k] = nil
    end
end

local function __genOrderedIndex(t)
    local orderedIndex = {}
    for key in pairs(t) do
        table.insert(orderedIndex, key)
    end
    table.sort(orderedIndex)
    return orderedIndex
end

local function orderedNext(t, state)
    -- Equivalent of the next function, but returns the keys in the alphabetic
    -- order. We use a temporary ordered key table that is stored in the
    -- table being iterated.

    local key = nil
    if state == nil then
        -- the first time, generate the index
        t.__orderedIndex = __genOrderedIndex(t)
        key = t.__orderedIndex[1]
    else
        -- fetch the next value
        for i = 1, table.getn(t.__orderedIndex) do
            if t.__orderedIndex[i] == state then
                key = t.__orderedIndex[i + 1]
            end
        end
    end

    if key then
        return key, t[key]
    end

    -- no more value to return, cleanup
    t.__orderedIndex = nil
    return
end

-- Equivalent of the pairs() function on tables.
--Allows ordered iteration
function utils.ordered_pairs(t)
    return orderedNext, t, nil
end

function utils.isarray(x)
    return (type(x) == "table" and x[1] ~= nil) and true or false
end

local function getiter(x)
    if utils.isarray(x) then
        return ipairs
    elseif type(x) == "table" then
        return pairs
    end
    error("expected table", 3)
end

function utils.remove(t, x)
    local iter = getiter(t)
    for i, v in iter(t) do
        if v == x then
            if utils.isarray(t) then
                table.remove(t, i)
                break
            else
                t[i] = nil
                break
            end
        end
    end
    return x
end

function utils.lookup_chain(t, v, ...)
    if v == nil then return t end

    t = t[v]

    if not t then
        return nil
    else
        return utils.lookup_chain(t, ...)
        end
    end

local function supertype(t1, t2)
    if type(t1) == "table" then
        return t1 == t2 or supertype(t1.super, t2)
    end
    return t1 == t2
end

function utils.encompasstype(var, _type)
    if type(var) == "table" then
        return ((var.prototype or "table") == _type) or supertype(var.super, _type)
    end
    return type(var) == _type
end

return utils